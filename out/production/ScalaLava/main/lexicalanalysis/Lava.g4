grammar Lava;

start : mainClass classDec* EOF ;

mainClass : 'class' IDENTIFIER '{' 'public' 'static' 'void' 'main'
    '(' 'String' '[' ']' IDENTIFIER ')'
     '{' statement '}' '}'
      | 'class' IDENTIFIER {notifyErrorListeners("Missing a '{' after the class definition");} 'public' 'static' 'void' 'main'
            '(' 'String' '[' ']' IDENTIFIER ')'
             '{' statement '}' '}'
      | 'class' IDENTIFIER '{' 'public' 'static' 'void' 'main'
            '(' 'String' '[' ']' IDENTIFIER ')'
             '{' statement '}' {notifyErrorListeners("Missing ending '}' for the main class");}
      | 'class' IDENTIFIER '{' 'public' 'static' 'void' 'main'
            '(' 'String' '[' ']' IDENTIFIER ')'
             {notifyErrorListeners("Missing '{' at the main method call");} statement '}' '}'
      | 'class' IDENTIFIER '{' 'public' 'static' 'void' 'main'
            '(' 'String' '[' ']' IDENTIFIER ')'
             '{' statement {notifyErrorListeners("Missing '}' at the end of the main method call");} '}'
      | {notifyErrorListeners("Missing the class identifier");} IDENTIFIER '{' 'public' 'static' 'void' 'main'
            '(' 'String' '[' ']' IDENTIFIER ')'
             '{' statement '}' '}'
      ;

classDec : 'class' IDENTIFIER ( extension )? '{'
    fieldDeclaration* methodDec* '}'
     | 'class' IDENTIFIER ( 'extends' IDENTIFIER )?
          {notifyErrorListeners("Missing '{' at the beginning of the class call");} fieldDeclaration* methodDec* '}'
     | 'class' IDENTIFIER ( 'extends' IDENTIFIER )? '{'
          fieldDeclaration* methodDec* {notifyErrorListeners("Missing '}' at the end of the class definition");}
    ;

extension : 'extends' IDENTIFIER ;

varDec : type IDENTIFIER
    ;

methodDec : 'public' methodReturn IDENTIFIER '(' ( methodParams )? ')' '{' localDeclaration* statement*
    aReturn? '}'
    | 'public' returnType IDENTIFIER '(' ( methodParams )? ')' {notifyErrorListeners("Missing '{' at the beginning of the method call");} localDeclaration*  statement*
          aReturn? '}'
    | 'public' returnType IDENTIFIER '(' ( type IDENTIFIER
          ( ',' type IDENTIFIER )* )? ')' '{' localDeclaration* statement* {notifyErrorListeners("Missing '}' at the end of the method call");}
          aReturn?
    | {notifyErrorListeners("Missing 'public' declaration for the method call");} returnType IDENTIFIER '(' ( type IDENTIFIER
          ( ',' type IDENTIFIER )* )? ')' '{' localDeclaration* statement*
          aReturn? '}'
    ;

localDeclaration:
    varDec
    ;

fieldDeclaration:
    varDec
    ;

methodReturn :
    returnType
    ;

methodParams :
    methodVarDec ( ',' methodVarDec )*
    ;

methodVarDec :
    type IDENTIFIER
    ;

type : 'int' '['']'
    | 'boolean'
    | 'int'
    | 'float'
    | IDENTIFIER
    ;

returnType : type
    | 'void'
    ;

aReturn : 'return' exp
    | {notifyErrorListeners("Missing a return key!");} exp
    | 'return' {notifyErrorListeners("Missing something to return!");}
    ;

statement :
     '{' statement* '}'
     #blockStatement
    | 'if' '(' exp ')' ifBlock 'else' eBlock
    #ifElseStatement
    | 'else' {notifyErrorListeners("There is just some random else for no reason!");}
    #ifError
    | 'while' '(' exp ')' wBlock
    #whileStatement
    | 'while' '(' {notifyErrorListeners("There is no expression to evaluate in this while loop!");} ')' wBlock
    #errorWhile
    | 'while' {notifyErrorListeners("Missing a '(' in the 'while' loop");} exp ')' wBlock
    #errorWhile
    | 'while' '(' exp {notifyErrorListeners("Missing a ')' in the 'while' loop");} wBlock
    #errorWhile
    | 'System.out.println' '(' exp ')'
    #consolePrint
    | 'System.out.println' {notifyErrorListeners("Missing a '(' in the System.out.println!");} exp ')'
    #errorConsole
    | 'System.out.println' '(' exp {notifyErrorListeners("Missing a ')' in the System.out.println!");}
    #errorConsole
    | IDENTIFIER '=' exp
    #assignmentStatement
    | IDENTIFIER '[' exp ']' '=' exp
    #arrayAssignmentStatement
    ;

ifBlock :
    statement
    ;

eBlock :
    statement
    ;

wBlock :
    statement
    ;

exp :
      exp '+' exp
    #addExpression
    | exp '-' exp
    #subtractionExpression
    | exp '*' exp
    #multiplyExpression
    | exp '&&' exp
    #andExpression
    | exp '<' exp
    #lessThanExpression
    | exp '>' exp
    #greaterThanExpression
    | exp '[' exp ']'
    #bracketExpression
    | exp '.' 'length'
    #lengthIdentifier
    | exp '.' IDENTIFIER '(' ( exp ( ',' exp )* )? ')'
    #methodCallExpression
    | INT
    #intLiteralExpression
    | 'true'
    #booleanTrueExpression
    | 'false'
    #booleanFalseExpression
    | IDENTIFIER
    #literalIdentifierExpression
    | FLOAT
    #floatExpression
    | 'this'
    #classAssignment
    | 'new' 'int' '[' exp ']'
    #arrayInitializerExpression
    | 'new' IDENTIFIER '('')'
    #objectInstantiationExpression
    | '!' exp
    #notExpression
    | '(' exp ')'
    #parenExperssion
    ;

INT : [0-9]+ ;

IDENTIFIER : [a-zA-Z] [a-zA-Z0-9_-]* ;

ENDLINE : [\n]+ -> skip ;

SPACE : [ ]+ -> skip ;

FLOAT : [0-9]+ '.' [0-9]+
      ;

WS : [\t\r]+ -> skip ;
COMMENT   : '/*' .*? '*/' -> skip ;
LINE_COMMENT : '//' .*? '\r'? '\n' -> skip;