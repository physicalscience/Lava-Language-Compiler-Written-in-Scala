package main.semanticanalysis

import scala.collection.mutable

/**
  * Table that holds all of the symbol information and scope information
  */
class Table {
  var tableMap = new mutable.HashMap[Symbol, Object]()
  var symbolMap = new mutable.HashMap[String, Symbol]()
  var scopeStack = new mutable.Stack[Symbol]()
  val begin = "**SCOPE**STACK**BEGIN**"

  /**
    * Puts a symbol into the table
    * @param s Takes the symbol
    * @param o Takes the object associated with it
    */
  def put(s: Symbol, o :Object) = {
    tableMap.put(s, o)
    symbolMap.put(s.toString, s)
    scopeStack.push(s)
  }

  /**
    * Gets an Object associated with a symbol from the tablemap
    * @param k takes in a Symbol as the key
    * @return returns an Optional[Object]
    */
  def get(k: Symbol): Object = {
    tableMap.get(k) match {
      case Some(o) => o
      case None => null
    }
  }

  /**
    * Gets the Symbol associated with some string that represents the symbol's name
    * @param s takes in a String
    * @return Returns an Optional[Symbol]
    */
  def get(s: String): Symbol = {
    symbolMap.get(s) match {
      case Some(o) => o
      case None => null
    }
  }

  /**
    * pushes a begin marker onto the stack
    */
  def beginScope = {
    scopeStack.push(Symbol.symbol(begin, new Marker))
  }

  /**
    * Ends the scope of the stack, which destroys all symbols until the stack hits a marker
    */
  def endScope = {
    while(!scopeStack.top.toString.equals(begin)) {
      val something = scopeStack.pop()
      tableMap.remove(something)
      symbolMap.remove(something.toString)
    }
    scopeStack.pop
  }
}
