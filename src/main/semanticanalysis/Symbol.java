package main.semanticanalysis;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Symbol class that holds all the information about a symbol
 */
public class Symbol {
    private String name;
    private Type type;

    /**
     * Symbol Constructor
     * @param n takes in the name
     * @param t takes in the type of the symbol
     */
    private Symbol(String n, Type t) { name = n; type = t; }
    private static Dictionary<String, Symbol> map = new Hashtable<>();

    /**
     * Returns the type of the symbol
     * @return returns a Type
     */
    public Type getType() { return type; }

    @Override
    public String toString() { return name; }

    /**
     * Takes in a string and type and puts it into the map
     * @param n takes in a name
     * @param t takes in a type
     * @return returns the Symbol
     */
    public static Symbol symbol(String n, Type t) {
        String u = n.intern();
        //Symbol s = map.get(u);
        //if(s == null) {
           Symbol s = new Symbol(u, t);
            map.put(u, s);
        //}
        return s;
    }
}
