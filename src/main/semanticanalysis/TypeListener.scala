package main.semanticanalysis

import java.util

import main.abstractsyntax.LavaBaseListener
import main.parsing.LavaParser._

import scala.collection.mutable

/**
  *  TypeListener Class that checks for type errors
  */
class TypeListener extends LavaBaseListener {
  val table = new Table
  var inClass: Boolean = false
  var inMethod: Boolean = false
  var returnType: Type = new Void
  var errorStack = new mutable.Stack[String]()
  var errorCounter = 0
  var classId: String = null

  /**
    * On entering the Start I go through and check for any inherited classes and associate the appropriate methods and global
    *   variables with them. It also sets all the internal tables for all the class for later type checking
    */
  override def enterStart(ctx: StartContext): Unit = {
    table.beginScope
    val superVarTable = new mutable.HashMap[String, util.ArrayList[Symbol]]()
    for(i <- 0 until ctx.classDec().size()) {
      val innerTable = new Table
      val s = Symbol.symbol(ctx.classDec(i).IDENTIFIER(0).getText, new Class(ctx.classDec(i).IDENTIFIER(0).getText, innerTable))
      table.put(s, ctx.classDec(i).IDENTIFIER(0))
      for(j <- 0 until ctx.classDec(i).methodDec().size()) {
        if(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().IDENTIFIER() == null) {
          if(check(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().getText).isInstanceOf[Class]) {
            val sym = Symbol.symbol(ctx.classDec(i).methodDec(j).IDENTIFIER(0).getText, table.get(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().getText).getType)
            innerTable.put(sym, ctx.classDec(i).methodDec(j).IDENTIFIER(0))
          }
          else {
            val sym = Symbol.symbol(ctx.classDec(i).methodDec(j).IDENTIFIER(0).getText, check(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().getText))
            innerTable.put(sym, ctx.classDec(i).methodDec(j).IDENTIFIER(0))
          }
        }
        else {
          if(check(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().getText).isInstanceOf[Class]) {
            val sym = Symbol.symbol(ctx.classDec(i).methodDec(j).IDENTIFIER(0).getText, table.get(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().IDENTIFIER().getText).getType)
            innerTable.put(sym, ctx.classDec(i).methodDec(j).IDENTIFIER(0))
          }
          else {
            val sym = Symbol.symbol(ctx.classDec(i).methodDec(j).IDENTIFIER(0).getText, check(ctx.classDec(i).methodDec(j).methodReturn().returnType().`type`().IDENTIFIER().getText))
            innerTable.put(sym, ctx.classDec(i).methodDec(j).IDENTIFIER(0))
          }
        }
      }
    }
    for(i <- 0 until ctx.classDec().size()) {
      superVarTable.put(ctx.classDec(i).IDENTIFIER(0).getText, new util.ArrayList[Symbol])
      for(j <- 0 until ctx.classDec(i).fieldDeclaration().size()) {
        if(check(ctx.classDec(i).fieldDeclaration(j).varDec().`type`().getText).isInstanceOf[Class]) {
          val sym = Symbol.symbol(ctx.classDec(i).fieldDeclaration(j).varDec().IDENTIFIER().getText, table.get(ctx.classDec(i).fieldDeclaration(j).varDec().`type`().getText).getType)
          superVarTable.get(ctx.classDec(i).IDENTIFIER(0).getText) match {
            case Some(o) => o.add(sym)
            case None => null
          }
        }
        else {
          val sym = Symbol.symbol(ctx.classDec(i).fieldDeclaration(j).varDec().IDENTIFIER().getText, check(ctx.classDec(i).fieldDeclaration(j).varDec().`type`().getText))
          superVarTable.get(ctx.classDec(i).IDENTIFIER(0).getText) match {
            case Some(o) => o.add(sym)
            case None => null
          }
        }

      }
    }
    for(i <- 0 until ctx.classDec().size()) {
      if(ctx.classDec(i).extension() != null) {
        val child = table.get(ctx.classDec(i).IDENTIFIER(0).getText)
        val superT = table.get(ctx.classDec(i).extension().IDENTIFIER().getText)
        val varList = superVarTable.get(ctx.classDec(i).extension().IDENTIFIER().getText)
        val superTable = superT.getType.asInstanceOf[Class].table.symbolMap
        for(j <- 0 until superTable.toList.size) {
          child.getType.asInstanceOf[Class].table.put(superTable.toList(j)._2, superTable.toList(j)._2.getType)
        }
        varList match {
          case Some(o) =>
            for(j <- 0 until o.size()) {
              child.getType.asInstanceOf[Class].table.put(o.get(j), ctx.classDec(i).extension().IDENTIFIER())
            }
          case None => null
        }


      }
    }
  }

  /**
    * End Start's Scope
    */
  override def exitStart(ctx: StartContext): Unit = {
    table.endScope
  }

  /**
    * When entering the main class, begin a new scope
    */
  override def enterMainClass(ctx: MainClassContext): Unit = {
    table.beginScope
    inClass = true
  }

  /**
    * when exiting the main class, end the scope
    */
  override def exitMainClass(ctx: MainClassContext): Unit = {
    table.endScope
    inClass = false
  }

  /**
    * When entering the class begin a new scope, set the class id, and load up the tables internal table to
    *   the global table
    */
  override def enterClassDec(ctx: ClassDecContext): Unit = {
    table.beginScope
    inClass = true
    classId = ctx.IDENTIFIER(0).getText
    val t = table.get(ctx.IDENTIFIER(0).getText).getType.asInstanceOf[Class].table.symbolMap.toList
    for(i <- 0 until t.size) {
      table.put(t(i)._2, t(i)._2.getType)
    }
  }

  /**
    *  when exiting the class, end the scope
    */
  override def exitClassDec(ctx: ClassDecContext): Unit = {
    table.endScope
    inClass = false
    classId = null
  }

  /**
    * When entering the method, check to see if you are in a class, begin the scope and check the parameter types and
    *   return type
    */
  override def enterMethodDec(ctx: MethodDecContext): Unit = {
    if(!inClass) {
      addToErrorStack("There was an error, no Class context for Method declaration: "
        + ctx.IDENTIFIER(0).getText + "!" + "\n" + "Line : " + ctx.getStart.getLine)
    }
    else {
      table.beginScope
      inMethod = true
      if(check(ctx.methodReturn().returnType().`type`().getText).isInstanceOf[Class]) {
        val sym = Symbol.symbol(ctx.IDENTIFIER(0).getText, table.get(ctx.methodReturn().returnType().`type`().getText).getType)
        returnType = table.get(ctx.methodReturn().returnType().`type`().getText).getType
        table.put(sym, ctx.IDENTIFIER(0))
      }
      else {
        val sym = Symbol.symbol(ctx.IDENTIFIER(0).getText, check(ctx.methodReturn().returnType().`type`().getText))
        table.put(sym, ctx.IDENTIFIER(0))
        returnType = check(ctx.methodReturn().returnType().`type`().getText)
      }

    }
  }


  /**
    * When exiting the method, end the scope
    */
  override def exitMethodDec(ctx: MethodDecContext): Unit = {
    table.endScope
    inMethod = false
  }

  /**
    * When exiting the variable declaration, check for some errors, and check its type
    */
  override def exitVarDec(ctx: VarDecContext): Unit = {
    if(!inMethod && !inClass) {
      addToErrorStack("There was an error, There is no context for var: " + ctx.IDENTIFIER().getText + "\n" + "on line: " + ctx.getStart.getLine)
    }
    if(table.get(ctx.IDENTIFIER().getText) != null) {
      addToErrorStack("You cannot declare a variable of the same name twice! \n on Line: " + ctx.start.getLine)
    }
    else {
          if(check(ctx.`type`().getText).isInstanceOf[Class]) {
            val varDec = Symbol.symbol(ctx.IDENTIFIER().getText, table.get(ctx.`type`().getText).getType)
            table.put(varDec, ctx.IDENTIFIER())
          }
          else {
            val varDec = Symbol.symbol(ctx.IDENTIFIER().getText, check(ctx.`type`().getText))
            table.put(varDec, ctx.IDENTIFIER())
          }
        }
    }

  /**
    * When exiting a return, make sure that the types are the same
    */
  override def exitAReturn(ctx: AReturnContext): Unit = {
    if(!table.get(ctx.exp().getText).getType.toString.equals(returnType.toString)) {
      addToErrorStack("The return type of a method does" +
        " not match the expected return type!" + "\n" + "Expected return type: " + returnType.toString + " || " + "actual return type: " + table.get(ctx.exp().getText).getType
       + "\n" + "on Line: " + ctx.getStart.getLine)
    }
  }

  /**
    * begin a scope when entering the block statement
    */
  override def enterBlockStatement(ctx: BlockStatementContext): Unit = {
    table.beginScope
  }

  /**
    *  end the scope when the block statement is exited
    */
  override def exitBlockStatement(ctx: BlockStatementContext): Unit = {
    table.endScope
  }

  /**
    * begin a scope when entering an if/else statement
    */
  override def enterIfElseStatement(ctx: IfElseStatementContext): Unit = {
    table.beginScope
  }

  /**
    * exit the if/else statement and check to makes sure the expression exists and that it evaluates to a boolean
    */
  override def exitIfElseStatement(ctx: IfElseStatementContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("You are trying to evaluate an expression " + ctx.exp().getText + " that doesn't exist! \n on Line: " + ctx.start.getLine)
    }
    else if(!table.get(ctx.exp().getText).getType.isInstanceOf[ABoolean]) {
      addToErrorStack("The expression in if/else statement does not evaluate to a type of Boolean! \n on Line: " + ctx.start.getLine)
    }
    table.endScope
  }

  /**
    * begin the scope when entering the while loop
    */
  override def enterWhileStatement(ctx: WhileStatementContext): Unit = {
    table.beginScope
  }

  /**
    * when exiting the while loop, check for a non-existent expression and check to make sure the expression evaluates
    *   to a boolean
    */
  override def exitWhileStatement(ctx: WhileStatementContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("You are trying to evaluate an expression " + ctx.exp().getText + " that doesn't exist! \n on Line: " + ctx.start.getLine)
    }
    else if(!table.get(ctx.exp().getText).getType.isInstanceOf[ABoolean]) {
      addToErrorStack("The expression in an if statement does not evaluate to a type of Boolean! \n on Line: " + ctx.start.getLine)
    }
    table.endScope
  }

  /**
    *  When exiting the assignment statement, make sure the types are good and check for other errors
    */
  override def exitAssignmentStatement(ctx: AssignmentStatementContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("You are trying to assign something that hasn't been declared to value: " + ctx.IDENTIFIER().getText + "\n" +
        "on line: " + ctx.start.getLine)
    }
    else if(table.get(ctx.IDENTIFIER().getText) == null) {
      addToErrorStack("You are trying to assign a value to a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
    }
    else {
      if(!table.get(ctx.IDENTIFIER().getText).getType.toString.equals(table.get(ctx.exp().getText).getType.toString)) {
        addToErrorStack("You cannot assign a type of: " + table.get(ctx.exp().getText).getType + " to a type of: " +
        table.get(ctx.IDENTIFIER().getText).getType + "\n on Line: " + ctx.start.getLine)
      }
      else {
       // assignment
      }
    }
  }

  /**
    * when exiting the array assignment, add a new symbol to the table
    */
  override def exitArrayAssignmentStatement(ctx: ArrayAssignmentStatementContext): Unit = {
    val array = ctx.IDENTIFIER().getText  + "[" + ctx.exp(0).getText + "]"
    val sym = Symbol.symbol(array, new Integer)
    table.put(sym, ctx.IDENTIFIER())
  }

  /**
    * put a new boolean onto the table
    */
  override def exitBooleanTrueExpression(ctx: BooleanTrueExpressionContext): Unit = {
    val sym = Symbol.symbol(ctx.getText, new ABoolean)
    table.put(sym, ctx)
  }

  /**
    * When exiting an object instantiation, add it to the table
    */
  override def exitObjectInstantiationExpression(ctx: ObjectInstantiationExpressionContext): Unit = {
    if(table.get(ctx.IDENTIFIER().getText) == null) {
      addToErrorStack("You are trying to instantiate an Object that doesn't exist! \n on Line: " + ctx.start.getLine)
    }
    else {
      val n = "new" + ctx.IDENTIFIER().getText + "()"
      val sym = Symbol.symbol(n, table.get(ctx.IDENTIFIER().getText).getType)
      table.put(sym, ctx.IDENTIFIER())
    }
  }

  /**
    * When exiting a class assignment, add it to the table
    */
  override def exitClassAssignment(ctx: ClassAssignmentContext): Unit = {
    if(classId == null) {
      addToErrorStack("you are trying to assign a variable a class when you have no reference! \n on Line: " + ctx.start.getLine)
    }
    else if(table.get(classId) == null) {
      addToErrorStack("There is no such class that you are trying to assign! \n on Line: " + ctx.start.getLine)
    }
    else {
      val s = Symbol.symbol(ctx.getText, table.get(classId).getType)
      table.put(s, ctx)
    }
  }

  /**
    * Add a new Int to the stack
    */
  override def enterIntLiteralExpression(ctx: IntLiteralExpressionContext): Unit = {
    if(!inClass && !inMethod) {
      addToErrorStack("You have an Integer with no context! : " + ctx.INT().getText + " at line: " + ctx.getStart.getLine)
    }
    else {
      val intLiteral = Symbol.symbol(ctx.INT().getText, new Integer)
      table.put(intLiteral, ctx.INT())
    }
  }

  /**
    * Add a float to the Table
    */
  override def enterFloatExpression(ctx: FloatExpressionContext): Unit = {
    if(!inClass && !inMethod) {
      addToErrorStack("You have a Float with no context! : " + ctx.FLOAT().getText + "\n" + "on Line: " + ctx.getStart.getLine)
    }
    else {
      val floatLiteral = Symbol.symbol(ctx.FLOAT().getText, new Floating)
      table.put(floatLiteral, ctx.FLOAT())
    }
  }

  /**
    * add a new method to the table
    */
  override def exitMethodCallExpression(ctx: MethodCallExpressionContext): Unit = {
    if(table.get(ctx.exp(0).getText) == null) {
      addToErrorStack("Your are trying to call a class that doesn't exist in the scope! \n on Line: " + ctx.start.getLine)
    }
    else {
      val c = table.get(ctx.exp(0).getText).getType.asInstanceOf[Class]
      if(c.table.get(ctx.IDENTIFIER().getText) == null) {
        addToErrorStack("There is no such method in the class! \n on Line: " + ctx.start.getLine)
      }
      else {
        val n = Symbol.symbol(ctx.getText, c.table.get(ctx.IDENTIFIER().getText).getType)
        table.put(n, ctx)
      }
    }
  }

  /**
    * When exiting the not expression, make sure that it is a boolean and add it to the stack
    */
  override def exitNotExpression(ctx: NotExpressionContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("The variable " + table.get(ctx.exp().getText) + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
    }
    else {
      val typeOf = table.get(ctx.exp().getText).getType
      if (!typeOf.isInstanceOf[ABoolean]) {
        addToErrorStack("It is impossible to apply the ! operation to a type of: " + typeOf + "\n" + "on Line: " + ctx.getStart.getLine)
      }
      else {
        val t = "!" + ctx.exp().getText
        val sym = Symbol.symbol(t, table.get(ctx.exp().getText).getType)
        table.put(sym, ctx.exp())
      }
    }
  }

  /**
    * When exiting the multiply expression make sure that the variables have some value set to them, make sure that
    *   the types are the same, and add it to the table
    */
  override def exitMultiplyExpression(ctx: MultiplyExpressionContext): Unit = {
    var errors: Boolean = false
    var typeOf: Type = null
    if(table.get(ctx.exp(0).getText) == null) {
      addToErrorStack("You are trying to * an expression with a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
      errors = true
    }
    else {
      typeOf = table.get(ctx.exp().get(0).getText).getType
      if (!typeOf.isInstanceOf[Integer] && !typeOf.isInstanceOf[Floating]) {
        addToErrorStack("You cannot multiply something that is a " + typeOf + "!" + "\n" + "on Line: " + ctx.getStart.getLine)
        errors = true
      }
      else {
        for (i <- 1 until ctx.exp().size()) {
          if (table.get(ctx.exp().get(i).getText) == null) {
            addToErrorStack("There was an error with The" +
              " variable : " + ctx.exp().get(i).getText + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else if (!table.get(ctx.exp().get(i).getText).getType.toString.equals(typeOf.toString)) {
            addToErrorStack("There was a type error for The" +
              " variable : " + ctx.exp().get(i).getText + " is of type : " + table.get(ctx.exp().get(i).getText).getType + " when the expected type is : " + typeOf
              + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else {
            // add the actual add operation
          }
        }
      }
    }
    if(!errors) {
      val builder = new StringBuilder
      for(i <- 0 until ctx.exp.size()) {
        builder.append(ctx.exp().get(i).getText)
        builder.append("*")
      }
      builder.deleteCharAt(builder.size - 1)
      val addSymbol = Symbol.symbol(builder.toString().trim, typeOf)
      table.put(addSymbol, ctx.exp())
    }
  }

  /**
    * When exiting the Greater than expression make sure that the variables have some value set to them, make sure that
    *   the types are the same, and add it to the table
    */
  override def exitGreaterThanExpression(ctx: GreaterThanExpressionContext): Unit = {
    var errors: Boolean = false
    if(table.get(ctx.exp(0).getText) == null) {
      addToErrorStack("You are trying to > an expression with a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
      errors = true
    }
    else {
      val typeOf = table.get(ctx.exp().get(0).getText).getType
      if (!typeOf.isInstanceOf[Integer] && !typeOf.isInstanceOf[Floating]) {
        addToErrorStack("You cannot apply the greater than operation to something that is a " + typeOf + "!" + "\n" + "on Line: " + ctx.getStart.getLine)
        errors = true
      }
      else {
        for (i <- 1 until ctx.exp().size()) {
          if (table.get(ctx.exp().get(i).getText) == null) {
            addToErrorStack("There was an error with The" +
              " variable : " + ctx.exp().get(i).getText + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else if (!table.get(ctx.exp().get(i).getText).getType.toString.equals(typeOf.toString)) {
            addToErrorStack("There was a type error for The" +
              " variable : " + ctx.exp().get(i).getText + " is of type : " + table.get(ctx.exp().get(i).getText).getType + " when the expected type is : " + typeOf
              + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else {

          }
        }
      }
    }
    if(!errors) {
      val builder = new StringBuilder
      for(i <- 0 until ctx.exp.size()) {
        builder.append(ctx.exp().get(i).getText)
        builder.append(">")
      }
      builder.deleteCharAt(builder.size - 1)
      val addSymbol = Symbol.symbol(builder.toString().trim, new ABoolean)
      table.put(addSymbol, ctx.exp())
    }
  }

  /**
    * Make sure that the index has some value and that the type is good then add it to the table
    *     */
  override def exitBracketExpression(ctx: BracketExpressionContext): Unit = {
    if(table.get(ctx.exp(1).getText) == null) {
      addToErrorStack("You are trying to grab an array index with a undefined variable! \n on line: " + ctx.start.getLine)
    }
    else if(!table.get(ctx.exp(1).getText).getType.isInstanceOf[Integer]) {
      addToErrorStack("You are trying to grab an array index with something that is of type: " + table.get(ctx.exp(1).getText).getType)
    }
    else {
      val st = ctx.exp(0).getText + "[" + ctx.exp(1).getText + "]"
      val sym = Symbol.symbol(st, new Integer)
      table.put(sym, ctx.exp(0))
    }

  }

  /**
    * Add the paren expression to the table
    */
  override def exitParenExperssion(ctx: ParenExperssionContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("you are trying to evaluate an expression with an variable that doesn't exist! \n on Line: " + ctx.start.getLine)
    }
    else {
      val parenExp = "(" + ctx.exp().getText + ")"
      val symb = Symbol.symbol(parenExp, table.get(ctx.exp().getText).getType)
      table.put(symb, ctx.exp())
    }
  }

  /**
    * Make sure the array exists and add the result to the table
    */
  override def exitLengthIdentifier(ctx: LengthIdentifierContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("You are trying to call length on a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
    }
    if(!table.get(ctx.exp().getText).getType.isInstanceOf[Array]) {
      addToErrorStack("You are trying to see the length of a variable that isn't an array! \n on line: " + ctx.start.getLine)
    }
    else {
      val st = ctx.exp().getText + ".length"
      val symbol = Symbol.symbol(st, new Integer)
      table.put(symbol, ctx)
    }
  }

  /**
    * When exiting the And expression make sure that the variables have some value set to them, make sure that
    *   the types are the same, and add it to the table
    */
  override def exitAndExpression(ctx: AndExpressionContext): Unit = {
    var errors: Boolean = false
    if(table.get(ctx.exp(0).getText) == null) {
      addToErrorStack("You are trying to && an expression with a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
      errors = true
    }
    else {
      val typeOf = table.get(ctx.exp().get(0).getText).getType
      if (!typeOf.isInstanceOf[ABoolean]) {
        addToErrorStack("You cannot apply the && operation to  something that is a " + typeOf + "!" + "\n" + "on Line: " + ctx.getStart.getLine)
        errors = true
      }
      else {
        for (i <- 1 until ctx.exp().size()) {
          if (table.get(ctx.exp().get(i).getText) == null) {
            addToErrorStack("There was an error with The" +
              " variable : " + ctx.exp().get(i).getText + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else if (!table.get(ctx.exp().get(i).getText).getType.toString.equals(typeOf.toString)) {
            addToErrorStack("There was a type error for The" +
              " variable : " + ctx.exp().get(i).getText + " is of type : " + table.get(ctx.exp().get(i).getText).getType + " when the expected type is : " + typeOf
              + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else {
            // add the actual && operation
          }
        }
      }
    }
    if(!errors) {
      val builder = new StringBuilder
      for(i <- 0 until ctx.exp.size()) {
        builder.append(ctx.exp().get(i).getText)
        builder.append("&&")
      }
      builder.deleteCharAt(builder.size - 1)
      builder.deleteCharAt(builder.size - 1)
      val addSymbol = Symbol.symbol(builder.toString().trim, new ABoolean)
      table.put(addSymbol, ctx.exp())
    }
  }

  /**
    * When exiting the Add expression make sure that the variables have some value set to them, make sure that
    *   the types are the same, and add it to the table
    */
  override def exitAddExpression(ctx: AddExpressionContext): Unit = {
    val typeOf = table.get(ctx.exp().get(0).getText).getType
    var error: Boolean = false
    if(!typeOf.isInstanceOf[Integer] && !typeOf.isInstanceOf[Floating]) {
      addToErrorStack("You cannot add something that is a " + typeOf + "!" + "\n" + "on Line: " + ctx.getStart.getLine)
      error = true
    }
    else {
      for (i <- 1 until ctx.exp().size()) {
        if (table.get(ctx.exp().get(i).getText) == null) {
          addToErrorStack("There was an error with The" +
            " variable : " + ctx.exp().get(i).getText + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
          error = true
        }
        else if (!table.get(ctx.exp().get(i).getText).getType.toString.equals(typeOf.toString)) {
          addToErrorStack("There was a type error for The" +
            " variable : " + ctx.exp().get(i).getText + " is of type : " + table.get(ctx.exp().get(i).getText).getType + " when the expected type is : " + typeOf
          + "\n" + "on Line: " + ctx.getStart.getLine)
          error = true
        }
        else {
          // add the actual add operation
        }
      }
    }
    if(!error) {
      val builder = new StringBuilder
      for(i <- 0 until ctx.exp.size()) {
        builder.append(ctx.exp().get(i).getText)
        builder.append("+")
      }
      builder.deleteCharAt(builder.size - 1)
      val addSymbol = Symbol.symbol(builder.toString().trim, typeOf)
      table.put(addSymbol, ctx.exp())
    }
  }

  /**
    * Add the initialized array to the table
    */
  override def exitArrayInitializerExpression(ctx: ArrayInitializerExpressionContext): Unit = {
    if(table.get(ctx.exp().getText) == null) {
      addToErrorStack("you are trying to initialize an array with something that doesn't exist!" + "\n" + "on Line: " + ctx.getStart.getLine)
    }
    else if(!table.get(ctx.exp().getText).getType.toString.equals("int")) {
      addToErrorStack("You are trying to initialize an array with something that is not an int" + "\n" + "on Line: " + ctx.getStart.getLine)
    }
    else {
      val sym = Symbol.symbol(ctx.getText, new Array(0))
      table.put(sym, ctx)
    }
  }

  /**
    * When exiting the subtraction expression make sure that the variables have some value set to them, make sure that
    *   the types are the same, and add it to the table
    */
  override def exitSubtractionExpression(ctx: SubtractionExpressionContext): Unit = {
    var errors: Boolean = false
    var typeOf: Type = null
    if(table.get(ctx.exp(0).getText) == null) {
      addToErrorStack("You are trying to > an expression with a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
      errors = true
    }
    else {
      typeOf = table.get(ctx.exp().get(0).getText).getType
      if (!typeOf.isInstanceOf[Integer] && !typeOf.isInstanceOf[Floating]) {
        addToErrorStack("You cannot subtract something that is a " + typeOf + "!" + "\n" + "on Line: " + ctx.getStart.getLine)
        errors = true
      }
      else {
        for (i <- 1 until ctx.exp().size()) {
          if (table.get(ctx.exp().get(i).getText) == null) {
            addToErrorStack("There was an error with The" +
              " variable : " + ctx.exp().get(i).getText + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else if (!table.get(ctx.exp().get(i).getText).getType.toString.equals(typeOf.toString)) {
            addToErrorStack("There was a type error for The" +
              " variable : " + ctx.exp().get(i).getText + " is of type : " + table.get(ctx.exp().get(i).getText).getType + " when the expected type is : " + typeOf
              + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else {
            // add the actual sub operation
          }
        }
      }
    }
    if(!errors) {
      val builder = new StringBuilder
      for(i <- 0 until ctx.exp.size()) {
        builder.append(ctx.exp().get(i).getText)
        builder.append("-")
      }
      builder.deleteCharAt(builder.size - 1)
      val addSymbol = Symbol.symbol(builder.toString().trim, typeOf)
      table.put(addSymbol, ctx.exp())
    }
  }

  /**
    * When exiting the less than expression make sure that the variables have some value set to them, make sure that
    *   the types are the same, and add it to the table
    */
  override def exitLessThanExpression(ctx: LessThanExpressionContext): Unit = {
    var errors: Boolean = false
    if(table.get(ctx.exp().get(0).getText) == null) {
      addToErrorStack("You are trying to < an expression with a variable that doesn't exist! \n on Line: " + ctx.start.getLine)
      errors = true
    }
    else {
      val typeOf = table.get(ctx.exp().get(0).getText).getType
      if (!typeOf.isInstanceOf[Integer] && !typeOf.isInstanceOf[Floating]) {
        addToErrorStack("You cannot apply the less than operation to something that is a " + typeOf + "!" + "\n" + "on Line: " + ctx.getStart.getLine)
        errors = true
      }
      else {
        for (i <- 1 until ctx.exp().size()) {
          if (table.get(ctx.exp().get(i).getText) == null) {
            addToErrorStack("There was an error with The" +
              " variable : " + ctx.exp().get(i).getText + " has not been declared!" + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else if (!table.get(ctx.exp().get(i).getText).getType.toString.equals(typeOf.toString)) {
            addToErrorStack("There was a type error for The" +
              " variable : " + ctx.exp().get(i).getText + " is of type : " + table.get(ctx.exp().get(i).getText).getType + " when the expected type is : " + typeOf
              + "\n" + "on Line: " + ctx.getStart.getLine)
            errors = true
          }
          else {
            // add the actual lessThan operation
          }
        }
      }
    }
    if(!errors) {
      val builder = new StringBuilder
      for(i <- 0 until ctx.exp.size()) {
        builder.append(ctx.exp().get(i).getText)
        builder.append("<")
      }
      builder.deleteCharAt(builder.size - 1)
      val addSymbol = Symbol.symbol(builder.toString().trim, new ABoolean)
      table.put(addSymbol, ctx.exp())
    }
  }

  /**
    * Add a boolean value to the table
    */
  override def exitBooleanFalseExpression(ctx: BooleanFalseExpressionContext): Unit = {
    val sym = Symbol.symbol(ctx.getText, new ABoolean)
    table.put(sym, ctx)
  }

  /**
    * When exiting the method variable declaration, add it to the table
    *     */
  override def exitMethodVarDec(ctx: MethodVarDecContext): Unit = {
    val t = check(ctx.`type`().getText)
    if(t.isInstanceOf[Class]) {
      val methParam = Symbol.symbol(ctx.IDENTIFIER().getText, table.get(ctx.`type`().IDENTIFIER().getText).getType)
      table.put(methParam, ctx.IDENTIFIER())
    }
    else {
      val methParam = Symbol.symbol(ctx.IDENTIFIER().getText, t)
      table.put(methParam, ctx.IDENTIFIER())
    }
  }

  /**
    * Simple type checking method
    * @param s takes in a String that is pattern matched
    * @return returns a Class that represents a type
    */
  def check(s: String): Type = {
    s match {
      case "int" => new Integer
      case "float" => new Floating
      case "boolean" => new ABoolean
      case "int[]" => new Array(0)
      case "void" => new Void
      case _ => new Class(null, null)
    }
  }

  /**
    * Print the error and exit the program
    * @param s Takes in the error message
    */
  def addToErrorStack(s: String) = {
    System.err.println("Compile halted because of a Type Error!")
    System.err.println(s)
    System.exit(1)
  }

}
