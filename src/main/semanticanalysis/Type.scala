package main.semanticanalysis

/**
  * Sealed Trait that represents different Types
  */
sealed trait Type

/**
  * Integer Class
  */
class Integer extends Type {
    val name = "int"
    val width = 4

    override def toString: String = name
  }

/**
  * Float Class
  */
class Floating extends Type {
    val name = "float"
    val width = 8

    override def toString: String = name
  }

/**
  * Boolean class
  */
  class ABoolean extends Type {
    val name = "boolean"
    val width = 1

    override def toString: String = name
  }

/**
  * Array class
  * @param s takes in the size
  */
  class Array(s: Int) extends Type{
    val name = "array"
    val size = s
    val width = size * 4

    override def toString: String = name
  }

/**
  * Class class
  * @param s takes in the string representation
  * @param t takes in the Class table associated with it
  */
  class Class(s: String, t: Table) extends Type {
    val identifier = s
    val table = t

    override def toString: String = identifier
  }

/**
  * Method class
  */
  class Method extends Type {}

/**
  * Marker Class
  */
  class Marker extends Type {}

/**
  * Void Class
  */
  class Void extends Type {}

