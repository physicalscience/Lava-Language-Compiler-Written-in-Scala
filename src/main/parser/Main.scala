package main.parser

import java.io.{File, FileInputStream}
import java.util

import jdk.internal.org.objectweb.asm.commons.Method
import main.codegeneration.{CodeGenerator, PreWalk}
import main.parsing.{ErrorListener, LavaLexer, LavaParser}
import main.semanticanalysis.TypeListener
import org.antlr.v4.runtime.atn.PredictionMode
import org.antlr.v4.runtime.tree.{ParseTree, ParseTreeWalker}
import org.antlr.v4.runtime.{ANTLRInputStream, CharStream, CommonTokenStream, DiagnosticErrorListener}

import scala.collection.mutable

/**
  * The Main insertion point for the compiler
  * At this point just runs all of the required MiniJava Files to pass Compiler
  */
object Main extends App {

  val testList = List("binarySearch.lava", "bubbleSort.lava", "factorial.lava", "linearSearch.lava",
    "linkedList.lava", "quickSort.lava", "treeSort.lava")

  testList.foreach(s => {
    val file = "/home/brandon/IdeaProjects/" + s
    val stream = new FileInputStream(new File(file))
    val antStream = new ANTLRInputStream(stream)
    val lexer = new LavaLexer(antStream.asInstanceOf[CharStream])
    val token = new CommonTokenStream(lexer)
    val parser = new LavaParser(token)

    parser.removeErrorListeners()

    parser.addErrorListener(new ErrorListener)
    parser.addErrorListener(new DiagnosticErrorListener())
    parser.getInterpreter.setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION)

    val tree = parser.start()

    val m = new mutable.HashMap[String, Method]()
    val o = new mutable.HashMap[String, String]()
    val e = new util.ArrayList[Method]()

    ParseTreeWalker.DEFAULT.walk(new TypeListener, tree)
    ParseTreeWalker.DEFAULT.walk(new PreWalk(m, o, e), tree)
    ParseTreeWalker.DEFAULT.walk(new CodeGenerator(m, o, e), tree)

  })
}
