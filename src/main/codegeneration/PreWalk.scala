package main.codegeneration

import java.util

import jdk.internal.org.objectweb.asm.Type
import jdk.internal.org.objectweb.asm.commons.Method
import main.abstractsyntax.LavaBaseListener
import main.parsing.LavaParser.{ClassDecContext, MethodDecContext}
import main.semanticanalysis.{ABoolean, Class, Floating, Integer, Void}

import scala.collection.mutable

/**
  * Little pre-walk that goes through all of the classes and methods and identifies them for the actual code generation walk
  */
class PreWalk(m: mutable.HashMap[String, Method], o: mutable.HashMap[String, String], e: util.ArrayList[Method]) extends LavaBaseListener {
  val ops = new Ops
  val methodTable = m
  val objectMap = o
  val scopeTable = new mutable.HashMap[String, String]()
  val parTable = new mutable.HashMap[String, Int]()
  var currentOType: Type = _

  /**
    * Entering the class declaration and setting all of the classes into the objectMap
    */
  override def enterClassDec(ctx: ClassDecContext): Unit = {
    val r = new mutable.HashMap[String, String]()
    if(ctx.extension() == null) {
      currentOType = Type.getObjectType(ctx.IDENTIFIER(0).getText)
      for(i <- 0 until ctx.methodDec().size()) {
        if(ctx.methodDec(i).methodParams() != null) objectMap.put(ctx.methodDec(i).IDENTIFIER(0).getText + ctx.methodDec(i).methodParams().methodVarDec().size(), ctx.IDENTIFIER(0).getText)
        else objectMap.put(ctx.methodDec(i).IDENTIFIER(0).getText + 0, ctx.IDENTIFIER(0).getText)
      }
    }
    else {
      currentOType = Type.getObjectType(ctx.extension().IDENTIFIER().getText)
      for(i <- 0 until ctx.methodDec().size()) {
        if(ctx.methodDec(i).methodParams() != null) objectMap.put(ctx.methodDec(i).IDENTIFIER(0).getText + ctx.methodDec(i).methodParams().methodVarDec().size(), ctx.extension().IDENTIFIER().getText)
        else objectMap.put(ctx.methodDec(i).IDENTIFIER(0).getText + 0, ctx.extension().IDENTIFIER().getText)
      }
    }
  }

  /**
    * when entering the method declaration, add all the methods to the method map
    */
  override def enterMethodDec(ctx: MethodDecContext): Unit = {
    var rt = Type.VOID_TYPE
    var argTypes = new Array[Type](0)
    var m: Method = null
    if(ctx.methodParams() != null) {
      val args = ctx.methodParams().methodVarDec()
      argTypes = new Array[Type](args.size())
      for (i <- 0 until args.size()) {
        scopeTable.put(args.get(i).IDENTIFIER().getText, "parm")
        parTable.put(args.get(i).IDENTIFIER().getText, i)
        if (args.get(i).`type`().IDENTIFIER() == null) {
          args.get(i).`type`().getText match {
            case "int" => argTypes(i) = Type.INT_TYPE
            case "int[]" => argTypes(i) = Type.getType(ops.intArray())
            case "boolean" => argTypes(i) = Type.BOOLEAN_TYPE
            case "float" => argTypes(i) = Type.FLOAT_TYPE
          }
        }
        else {
          argTypes(i) = Type.getObjectType(args.get(i).`type`().IDENTIFIER().getText)
        }
      }
    }
    if (!check(ctx.methodReturn().getText).isInstanceOf[Void]) {
      if (ctx.methodReturn().returnType().`type`().IDENTIFIER() == null) {
        ctx.methodReturn().returnType().`type`().getText match {
          case "int" => rt = Type.INT_TYPE
          case "int[]" => rt = Type.getType(ops.intArray())
          case "boolean" => rt = Type.BOOLEAN_TYPE
          case "float" => rt = Type.FLOAT_TYPE
        }
      }
      else rt = Type.getObjectType(ctx.methodReturn().returnType().`type`().IDENTIFIER().getText)
    }

    m = new Method(ctx.IDENTIFIER(0).getText, rt, argTypes)
    e.add(m)
    methodTable.put(ctx.IDENTIFIER(0).getText + currentOType.getDescriptor, m)
  }

  /**
    * little checker method to figure out types from strings
    * @param s Takes in a string
    * @return returns a class representation of the type
    */
  def check(s: String) = {
    s match {
      case "int" => new Integer
      case "float" => new Floating
      case "boolean" => new ABoolean
      case "void" => new Void
      case _ => new Class(null, null)
    }
  }
}
