package main.codegeneration;

import org.objectweb.asm.Opcodes;

import java.io.PrintStream;

/**
 * Little helper Java class that returned Java things I needed for successful Bytecode generation
 */
public class Ops implements Opcodes {
    public int v1_1() { return V1_1; }
    public int acc_public() { return ACC_PUBLIC; }
    public int acc_static() { return ACC_STATIC; }
    public int acc_protected() { return ACC_PROTECTED; }
    public float getFloat(String f) { return Float.parseFloat(f); }
    public boolean getBoolean(String b) { return Boolean.parseBoolean(b); }
    public Class<Object> object() { return Object.class; }
    public Class<System> system() { return System.class; }
    public Class<PrintStream> printStream() { return PrintStream.class; }
    public Class<int[]> intArray() { return int[].class; }
}
