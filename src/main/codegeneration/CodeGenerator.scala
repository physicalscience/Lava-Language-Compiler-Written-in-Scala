package main.codegeneration

import java.io.{File, FileNotFoundException, FileOutputStream, IOException}
import java.util

import jdk.internal.org.objectweb.asm.{ClassWriter, Label, Type}
import jdk.internal.org.objectweb.asm.commons.{GeneratorAdapter, Method}
import main.abstractsyntax.LavaBaseListener
import main.parsing.LavaParser._
import main.semanticanalysis.{ABoolean, Class, Floating, Integer, Table, Void}

import scala.collection.mutable

/**
  * This class generated the JavaByte code for the lava file being compiled
  */
class CodeGenerator(m: mutable.HashMap[String, Method], o: mutable.HashMap[String, String], e: util.ArrayList[Method]) extends LavaBaseListener {
  val table = new Table
  val ops = new Ops
  var methodGenerator: GeneratorAdapter = _
  var cw: ClassWriter = _
  var stream: FileOutputStream = _
  val lStack = new mutable.Stack[Label]
  val methodTable = m
  val objectMap = o
  val varTable = new mutable.HashMap[String, Type]()
  val localTable = new mutable.HashMap[String, Int]()
  val scopeTable = new mutable.HashMap[String, String]()
  val parTable = new mutable.HashMap[String, Int]()
  val litTable = new mutable.HashMap[String, Any]()
  var classType: Type = _
  var methodType: Type = _
  var inMethod: Boolean = false
  var inClass: Boolean = false

  /**
    * Stepping into the main class and invoking it and the generating the Main method
    */
  override def enterMainClass(ctx: MainClassContext): Unit = {
    inClass = true
    inMethod = true
    classType = Type.getObjectType(ctx.IDENTIFIER(0).getText)
    cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES)
    cw.visit(ops.v1_1(),ops.acc_public(), ctx.IDENTIFIER(0).getText, null, "java/lang/Object", null)
    methodGenerator = new GeneratorAdapter(ops.acc_public(), init(),null, null, cw)

    methodGenerator.loadThis()
    methodGenerator.invokeConstructor(Type.getType(ops.`object`()), init())
    methodGenerator.returnValue()
    methodGenerator.endMethod()

    methodGenerator = new GeneratorAdapter(ops.acc_public() + ops.acc_static(), Method.getMethod("void main (String[])"), null, null, cw)
  }

  /**
    * exiting the main class
    */
  override def exitMainClass(ctx: MainClassContext): Unit = {
    inClass = false
    inMethod = false
    methodGenerator.returnValue()
    methodGenerator.endMethod()

    cw.visitEnd()

    try {
      stream = new FileOutputStream(new File(ctx.IDENTIFIER(0).getText + ".class"))
      stream.write(cw.toByteArray)
      stream.close()
    }catch {
      case f: FileNotFoundException =>
        System.err.println("File was not Found!")
        System.exit(1)
      case o: IOException =>
        o.printStackTrace()
        System.exit(1)
    }
  }

  /**
    * stepping into a class declaration
    * setting up the classWriter for it and
    * initializing the class either as an Object or a subclass of another object
    */
  override def enterClassDec(ctx: ClassDecContext): Unit = {
    inClass = true
    classType = Type.getObjectType(ctx.IDENTIFIER(0).getText)
    cw = new ClassWriter(ClassWriter.COMPUTE_MAXS)
    if(ctx.extension() == null) cw.visit(ops.v1_1(), ops.acc_public(), ctx.IDENTIFIER(0).getText, null, "java/lang/Object", null)
    else cw.visit(ops.v1_1(), ops.acc_public(), ctx.IDENTIFIER(0).getText, null, ctx.extension().IDENTIFIER().getText, null)

    methodGenerator = new GeneratorAdapter(ops.acc_public(), init(), null, null, cw)
    methodGenerator.loadThis()
    if(ctx.extension() == null) methodGenerator.invokeConstructor(Type.getObjectType("java/lang/Object"), init())
    else methodGenerator.invokeConstructor(Type.getObjectType(ctx.extension().IDENTIFIER().getText), init())
    methodGenerator.returnValue()
    methodGenerator.endMethod()
  }

  /**
    * Exiting the class and writing it to a file
    */
  override def exitClassDec(ctx: ClassDecContext): Unit = {
    inClass = false
    cw.visitEnd()
    val j = cw.toByteArray

    try {
      stream = new FileOutputStream(new File(ctx.IDENTIFIER(0).getText + ".class"))
      stream.write(cw.toByteArray)
      stream.close()
    }catch {
      case f: FileNotFoundException =>
        System.err.println("File was not Found!")
        System.exit(1)
      case o: IOException =>
        o.printStackTrace()
        System.exit(1)
    }
  }

  /**
    * When exiting a local declaration I figure out the type of the variable being set and
    *  add it to some maps for later reference
    */
  override def exitLocalDeclaration(ctx: LocalDeclarationContext): Unit = {
    var t = Type.INT_TYPE
    if(ctx.varDec().`type`().IDENTIFIER() == null) {
      val ty = ctx.varDec().`type`().getText
      ty match {
        case "int" => t = Type.INT_TYPE
        case "int[]" => t = Type.getType(ops.intArray())
        case "boolean" => t = Type.BOOLEAN_TYPE
        case "float" => t = Type.FLOAT_TYPE
      }
    }
    else t = Type.getObjectType(ctx.varDec().`type`().IDENTIFIER().getText)

    localTable.put(ctx.varDec().IDENTIFIER().getText, methodGenerator.newLocal(t))
    varTable.put(ctx.varDec().IDENTIFIER().getText, t)
    scopeTable.put(ctx.varDec().IDENTIFIER().getText, "local")
  }

  /**
    * When exiting a field declaration I figure out its type and add it to
    *  some maps for later reference
    */
  override def exitFieldDeclaration(ctx: FieldDeclarationContext): Unit = {
    var t = Type.INT_TYPE
    if(ctx.varDec().`type`().IDENTIFIER() == null) {
      val ty = ctx.varDec().`type`().getText
      ty match {
        case "int" => t = Type.INT_TYPE
        case "int[]" => t = Type.getType(ops.intArray())
        case "boolean" => t = Type.BOOLEAN_TYPE
        case "float" => t = Type.FLOAT_TYPE
      }
    }
    else t = Type.getObjectType(ctx.varDec().`type`().IDENTIFIER().getText)

    scopeTable.put(ctx.varDec().IDENTIFIER().getText, "field")
    varTable.put(ctx.varDec().IDENTIFIER().getText, t)
    cw.visitField(ops.acc_protected(), ctx.varDec().IDENTIFIER().getText, t.getDescriptor, null, null)
    cw.visitEnd()
  }

  /**
    * The only thing I care about when entering the assignment statement is figuring out if the
    *   thing being assigned is a field type, which if it is I load up 'this' onto the stack for
    *   when I exit the assignment statement
    */
  override def enterAssignmentStatement(ctx: AssignmentStatementContext): Unit = {
    scopeTable.get(ctx.IDENTIFIER().getText) match {
      case Some(t) => t match {
        case "field" =>
          methodGenerator.loadThis()
        case _ => //do nothing
      }
      case None => // do nothing
    }
  }

  /**
    * On the exit of the assignment depending on what the Identifier being assigned is, I will store the
    *   assignment accordingly
    */
  override def exitAssignmentStatement(ctx: AssignmentStatementContext): Unit = {
    scopeTable.get(ctx.IDENTIFIER().getText) match {
      case Some(t) => t match {
        case "field" =>
          varTable.get(ctx.IDENTIFIER().getText) match {
            case Some(v) =>
              methodGenerator.putField(classType, ctx.IDENTIFIER().getText, v)
            case None => // do nothing
          }
        case "parm" =>
          parTable.get(ctx.IDENTIFIER().getText) match {
            case Some(p) => methodGenerator.storeArg(p)
            case None => System.err.println("There was an error setting a parameter \n on Line: " + ctx.getStart.getLine)
          }
        case "local" =>
          localTable.get(ctx.IDENTIFIER().getText) match {
            case Some(l) => varTable.get(ctx.IDENTIFIER().getText) match {
              case Some(v) => methodGenerator.storeLocal(l, v)
              case None => System.err.println("There was an error Setting a local variable assignment \n on line: " + ctx.getStart.getLine)
            }
            case None =>
              System.err.println("There was an error finding a variable \n on line: " + ctx.getStart.getLine)
          }
      }
      case None =>
        System.err.println("There was an error Setting a variable assignment \n on line: " + ctx.getStart.getLine)
    }
  }

  /**
    * When entering a method declaration I figure out the parameter types and the return type, use that do delalare
    *   a new method, and then declare a new Generator Adapter
    */
  override def enterMethodDec(ctx: MethodDecContext): Unit = {
    inMethod = true
    var rt = Type.VOID_TYPE
    var argTypes = new Array[Type](0)
    if (ctx.methodParams() != null) {
      val args = ctx.methodParams().methodVarDec()
      argTypes = new Array[Type](args.size())
      for (i <- args.toArray.indices) {
        scopeTable.put(args.get(i).IDENTIFIER().getText, "parm")
        parTable.put(args.get(i).IDENTIFIER().getText, i)
        if (args.get(i).`type`().IDENTIFIER() == null) {
          args.get(i).`type`().getText match {
            case "int" =>
              argTypes(i) = Type.INT_TYPE
            case "int[]" =>
              argTypes(i) = Type.getType(ops.intArray())
            case "boolean" =>
              argTypes(i) = Type.BOOLEAN_TYPE
            case "float" =>
              argTypes(i) = Type.FLOAT_TYPE
          }
        }
        else {
          argTypes(i) = Type.getObjectType(args.get(i).`type`().IDENTIFIER().getText)
        }
      }
    }
    if (!check(ctx.methodReturn().getText).isInstanceOf[Void]) {
      if (ctx.methodReturn().returnType().`type`().IDENTIFIER() == null) {
        ctx.methodReturn().returnType().`type`().getText match {
          case "int" =>
            rt = Type.INT_TYPE
          case "int[]" =>
            rt = Type.getType(ops.intArray())
          case "boolean" =>
            rt = Type.BOOLEAN_TYPE
          case "float" =>
            rt = Type.FLOAT_TYPE
        }
      }
      else {
        rt = Type.getObjectType(ctx.methodReturn().returnType().`type`().IDENTIFIER().getText)
      }
    }
    val m = new Method(ctx.IDENTIFIER(0).getText, rt, argTypes)
    methodGenerator = new GeneratorAdapter(ops.acc_public(), m, null, null, cw)
  }

  /**
    * exiting the method, returning its value and ending the method
    */
  override def exitMethodDec(ctx: MethodDecContext): Unit = {
    inMethod = false
    methodGenerator.returnValue()
    methodGenerator.endMethod()
  }


  /**
    * entering the Array Assignment, figuring out the the scape of the Identifier and loading it
    */
  override def enterArrayAssignmentStatement(ctx: ArrayAssignmentStatementContext): Unit = {
    scopeTable.get(ctx.IDENTIFIER().getText) match {
      case Some(t) => t match {
        case "field" =>
          varTable.get(ctx.IDENTIFIER().getText) match {
            case Some(v) =>
              methodGenerator.loadThis()
              methodGenerator.getField(classType, ctx.IDENTIFIER().getText, v)
            case None => // do nothing
          }
        case "parm" =>
          parTable.get(ctx.IDENTIFIER().getText) match {
            case Some(p) => methodGenerator.loadArg(p)
            case None => System.err.println("There was an error setting a parameter \n on Line: " + ctx.getStart.getLine)
          }
        case "local" =>
          localTable.get(ctx.IDENTIFIER().getText) match {
            case Some(l) => varTable.get(ctx.IDENTIFIER().getText) match {
              case Some(v) => methodGenerator.loadLocal(l, v)
              case None => System.err.println("There was an error Setting a local variable assignment \n on line: " + ctx.getStart.getLine)
            }
            case None =>
              System.err.println("There was an error finding a variable \n on line: " + ctx.getStart.getLine)
          }
      }
      case None =>
        System.err.println("There was an error Setting a variable assignment \n on line: " + ctx.getStart.getLine)
    }
  }

  /**
    * when I exit the array assignment I store the the Int on the stack into the array
    */
  override def exitArrayAssignmentStatement(ctx: ArrayAssignmentStatementContext): Unit = {
    methodGenerator.arrayStore(Type.INT_TYPE)
  }


  /**
    * On entering the if/else statement I make some labels and push them into my
    * label stack
    */
  override def enterIfElseStatement(ctx: IfElseStatementContext): Unit = {
    val enter = methodGenerator.newLabel()
    val exit = methodGenerator.newLabel()

    lStack.push(exit)
    lStack.push(enter)
    lStack.push(exit)
    lStack.push(enter)
  }

  /**
    * Comparing the top integer stack
    */
  override def enterIfBlock(ctx: IfBlockContext): Unit = {
    methodGenerator.ifZCmp(GeneratorAdapter.EQ, lStack.pop())
  }

  /**
    * On exit pop the labelstack and go there
    */
  override def exitIfBlock(ctx: IfBlockContext): Unit = {
    methodGenerator.goTo(lStack.pop())
  }

  /**
    * Entering the else block so I mark the top label in my stack
    */
  override def enterEBlock(ctx: EBlockContext): Unit = {
    methodGenerator.mark(lStack.pop())
  }

  /**
    * on exit I mark the last label in the stack
    */
  override def exitEBlock(ctx: EBlockContext): Unit = {
    methodGenerator.mark(lStack.pop())
  }


  /**
    * When I enter the while loop I make some labels and pop them onto the stack
    */
  override def enterWhileStatement(ctx: WhileStatementContext): Unit = {
    val enterWhile = methodGenerator.mark()
    val exitWhile = methodGenerator.newLabel()
    lStack.push(exitWhile)
    lStack.push(enterWhile)
    lStack.push(exitWhile)
  }


  /**
    * When I actually enter the while block I compare the top of the integer stack
    */
  override def enterWBlock(ctx: WBlockContext): Unit = {
    methodGenerator.ifZCmp(GeneratorAdapter.EQ, lStack.pop())
  }


  /**
    * on my exit I goto the next label in the stack then mark the last label
    */
  override def exitWhileStatement(ctx: WhileStatementContext): Unit = {
    methodGenerator.goTo(lStack.pop())
    methodGenerator.mark(lStack.pop())
  }

  /**
    * doing an and to the stack
    */
  override def exitAndExpression(ctx: AndExpressionContext): Unit = {
    methodGenerator.math(GeneratorAdapter.AND, Type.BOOLEAN_TYPE)
  }


  /**
    * doing an add to the stack
    */
  override def exitAddExpression(ctx: AddExpressionContext): Unit = {
    varTable.get(ctx.exp(0).getText) match {
      case Some(i) =>
        i match {
          case Type.FLOAT_TYPE =>
            methodGenerator.math(GeneratorAdapter.ADD, Type.FLOAT_TYPE)
            println("adding floats")
          case _ =>
            methodGenerator.math(GeneratorAdapter.ADD, Type.INT_TYPE)
        }
      case None =>
    }
  }


  /**
    * doing a subtract to the stack
    */
  override def exitSubtractionExpression(ctx: SubtractionExpressionContext): Unit = {
    varTable.get(ctx.exp(0).getText) match {
      case Some(i) =>
        i match {
          case Type.FLOAT_TYPE =>
            methodGenerator.math(GeneratorAdapter.SUB, Type.FLOAT_TYPE)
          case _ =>
            methodGenerator.math(GeneratorAdapter.SUB, Type.INT_TYPE)
        }
      case None =>
    }
  }


  /**
    * doing a multiply on the stack
    */
  override def exitMultiplyExpression(ctx: MultiplyExpressionContext): Unit = {
    varTable.get(ctx.exp(0).getText) match {
      case Some(i) =>
        i match {
          case Type.FLOAT_TYPE =>
            methodGenerator.math(GeneratorAdapter.MUL, Type.FLOAT_TYPE)
          case _ =>
            methodGenerator.math(GeneratorAdapter.MUL, Type.INT_TYPE)
        }
      case None =>
    }
  }


  /**
    * on exit of a array load put it on the stack
    */
  override def exitBracketExpression(ctx: BracketExpressionContext): Unit = {
    methodGenerator.arrayLoad(Type.INT_TYPE)
  }


  /**
    * Loading the length of the array onto the stack
    */
  override def exitLengthIdentifier(ctx: LengthIdentifierContext): Unit = {
    methodGenerator.arrayLength()
  }

  /**
    * In a method call I search for the method's owner in the object map then grab the actual Method
    *  from the method table. After doing all that I invoke the method
    */
  override def exitMethodCallExpression(ctx: MethodCallExpressionContext): Unit = {
    val methodParSize = ctx.exp().size() - 1

    objectMap.get(ctx.IDENTIFIER().getText + methodParSize) match {
      case Some(o) =>
        methodTable.get(ctx.IDENTIFIER().getText + Type.getObjectType(o).getDescriptor) match {
          case Some(m) =>
            methodGenerator.invokeVirtual(Type.getObjectType(o), m)
          case None =>
        }
      case None =>
    }
  }

  /**
    * on exit of a 'this' assignment, load this
    */
  override def exitClassAssignment(ctx: ClassAssignmentContext): Unit = {
    methodGenerator.loadThis()
  }

  /**
    * on the exit of an array initializer, initialize a new array
    */
  override def exitArrayInitializerExpression(ctx: ArrayInitializerExpressionContext): Unit = {
    methodGenerator.newArray(Type.INT_TYPE)
  }

  /**
    * When entering an object instantiation, create a new instance and invoke it
    */
  override def enterObjectInstantiationExpression(ctx: ObjectInstantiationExpressionContext): Unit = {
    val o = Type.getObjectType(ctx.IDENTIFIER().getText)
    methodGenerator.newInstance(o)
    methodGenerator.dup()
    methodGenerator.invokeConstructor(o, init())
  }


  /**
    * Not the top of the stack
    */
  override def exitNotExpression(ctx: NotExpressionContext): Unit = {
    methodGenerator.not()
  }


  /**
    * When entering console print, get the actual System.out static
    */
  override def enterConsolePrint(ctx: ConsolePrintContext): Unit = {
    methodGenerator.getStatic(Type.getType(ops.system()), "out", Type.getType(ops.printStream()))
  }

  /**
    * on exit of the console print, actually print
    */
  override def exitConsolePrint(ctx: ConsolePrintContext): Unit = {
    var happened = false
    var counter = 0
    while(!happened) {
      varTable.get(ctx.exp().children.get(counter).getText) match {
        case Some(v) =>
          v match {
            case Type.FLOAT_TYPE =>
              methodGenerator.invokeVirtual(Type.getType(ops.printStream()), Method.getMethod("void println(float)"))
              happened = true

            case _ =>
              methodGenerator.invokeVirtual(Type.getType(ops.printStream()), Method.getMethod("void println(int)"))
              happened = true
          }
        case None =>
      }
      counter += 1
      if(counter.equals(ctx.exp.children.size)) happened = true
    }

  }

  /**
    * on the exit of the less than expression, create two labels, compare on the stack,
    *   and based on that do some business with the labels on the stack
    */
  override def exitLessThanExpression(ctx: LessThanExpressionContext): Unit = {
    val t = methodGenerator.newLabel()
    val e = methodGenerator.newLabel()

    methodGenerator.ifCmp(Type.INT_TYPE, GeneratorAdapter.LT, t)
    methodGenerator.push(false)
    methodGenerator.goTo(e)
    methodGenerator.mark(t)
    methodGenerator.push(true)
    methodGenerator.mark(e)
  }

  /**
    *  when exiting a literal identifier expression, figure out its scope and load it
    */
  override def exitLiteralIdentifierExpression(ctx: LiteralIdentifierExpressionContext): Unit = {
    scopeTable.get(ctx.IDENTIFIER().getText) match {
      case Some(s) =>
        s match {
          case "field" =>
            varTable.get(ctx.IDENTIFIER().getText) match {
              case Some(v) =>
                methodGenerator.loadThis()
                methodGenerator.getField(classType, ctx.IDENTIFIER().getText, v)
              case None => //do nothing
            }
          case "parm" =>
            parTable.get(ctx.IDENTIFIER().getText) match {
              case Some(p) =>
                methodGenerator.loadArg(p)
              case None => //nothing
            }
          case "local" =>
            localTable.get(ctx.IDENTIFIER().getText) match {
              case Some(l) =>
                varTable.get(ctx.IDENTIFIER().getText) match {
                  case Some(v) =>
                    methodGenerator.loadLocal(l, v)
                  case None => //nothing
                }
              case None => //nothing
            }
        }
      case None => //nothing
    }

  }

  /**
    * Push an integer onto the stack
    */
  override def enterIntLiteralExpression(ctx: IntLiteralExpressionContext): Unit = {
    methodGenerator.push(Integer.parseInt(ctx.INT().getText))
  }


  /**
    * push a float onto the stack
    */
  override def enterFloatExpression(ctx: FloatExpressionContext): Unit = {
    methodGenerator.push(ops.getFloat(ctx.FLOAT().getText))
  }

  /**
    * push a boolean value of true onto the stack
    */
  override def enterBooleanTrueExpression(ctx: BooleanTrueExpressionContext): Unit = {
    methodGenerator.push(true)
  }

  /**
    * push a boolean value of false onto the stack
    */
  override def enterBooleanFalseExpression(ctx: BooleanFalseExpressionContext): Unit = {
    methodGenerator.push(false)
  }

  /**
    * basic init method for initializing objects and such
    * @return returns the init method
    */
  def init(): Method = { Method.getMethod("void <init> ()") }

  /**
    * Simple type checking method
    * @param s takes in a String that is pattern matched
    * @return returns a Class that represents a type
    */
  def check(s: String) = {
    s match {
      case "int" => new Integer
      case "float" => new Floating
      case "boolean" => new ABoolean
      case "void" => new Void
      case _ => new Class(null, null)
    }
  }
}
