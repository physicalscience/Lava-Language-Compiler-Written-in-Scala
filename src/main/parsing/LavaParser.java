// Generated from /home/brandon/IdeaProjects/ScalaLava/src/main/lexicalanalysis/Lava.g4 by ANTLR 4.5.3
package main.parsing;
import main.abstractsyntax.LavaListener;
import main.abstractsyntax.LavaVisitor;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LavaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, INT=37, IDENTIFIER=38, 
		ENDLINE=39, SPACE=40, FLOAT=41, WS=42, COMMENT=43, LINE_COMMENT=44;
	public static final int
		RULE_start = 0, RULE_mainClass = 1, RULE_classDec = 2, RULE_extension = 3, 
		RULE_varDec = 4, RULE_methodDec = 5, RULE_localDeclaration = 6, RULE_fieldDeclaration = 7, 
		RULE_methodReturn = 8, RULE_methodParams = 9, RULE_methodVarDec = 10, 
		RULE_type = 11, RULE_returnType = 12, RULE_aReturn = 13, RULE_statement = 14, 
		RULE_ifBlock = 15, RULE_eBlock = 16, RULE_wBlock = 17, RULE_exp = 18;
	public static final String[] ruleNames = {
		"start", "mainClass", "classDec", "extension", "varDec", "methodDec", 
		"localDeclaration", "fieldDeclaration", "methodReturn", "methodParams", 
		"methodVarDec", "type", "returnType", "aReturn", "statement", "ifBlock", 
		"eBlock", "wBlock", "exp"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'class'", "'{'", "'public'", "'static'", "'void'", "'main'", "'('", 
		"'String'", "'['", "']'", "')'", "'}'", "'extends'", "','", "'int'", "'boolean'", 
		"'float'", "'return'", "'if'", "'else'", "'while'", "'System.out.println'", 
		"'='", "'+'", "'-'", "'*'", "'&&'", "'<'", "'>'", "'.'", "'length'", "'true'", 
		"'false'", "'this'", "'new'", "'!'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, "INT", "IDENTIFIER", "ENDLINE", "SPACE", "FLOAT", "WS", "COMMENT", 
		"LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Lava.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LavaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public MainClassContext mainClass() {
			return getRuleContext(MainClassContext.class,0);
		}
		public TerminalNode EOF() { return getToken(LavaParser.EOF, 0); }
		public List<ClassDecContext> classDec() {
			return getRuleContexts(ClassDecContext.class);
		}
		public ClassDecContext classDec(int i) {
			return getRuleContext(ClassDecContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			mainClass();
			setState(42);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(39);
				classDec();
				}
				}
				setState(44);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(45);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainClassContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(LavaParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(LavaParser.IDENTIFIER, i);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public MainClassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainClass; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMainClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMainClass(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMainClass(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainClassContext mainClass() throws RecognitionException {
		MainClassContext _localctx = new MainClassContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_mainClass);
		try {
			setState(155);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(47);
				match(T__0);
				setState(48);
				match(IDENTIFIER);
				setState(49);
				match(T__1);
				setState(50);
				match(T__2);
				setState(51);
				match(T__3);
				setState(52);
				match(T__4);
				setState(53);
				match(T__5);
				setState(54);
				match(T__6);
				setState(55);
				match(T__7);
				setState(56);
				match(T__8);
				setState(57);
				match(T__9);
				setState(58);
				match(IDENTIFIER);
				setState(59);
				match(T__10);
				setState(60);
				match(T__1);
				setState(61);
				statement();
				setState(62);
				match(T__11);
				setState(63);
				match(T__11);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(65);
				match(T__0);
				setState(66);
				match(IDENTIFIER);
				notifyErrorListeners("Missing a '{' after the class definition");
				setState(68);
				match(T__2);
				setState(69);
				match(T__3);
				setState(70);
				match(T__4);
				setState(71);
				match(T__5);
				setState(72);
				match(T__6);
				setState(73);
				match(T__7);
				setState(74);
				match(T__8);
				setState(75);
				match(T__9);
				setState(76);
				match(IDENTIFIER);
				setState(77);
				match(T__10);
				setState(78);
				match(T__1);
				setState(79);
				statement();
				setState(80);
				match(T__11);
				setState(81);
				match(T__11);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(83);
				match(T__0);
				setState(84);
				match(IDENTIFIER);
				setState(85);
				match(T__1);
				setState(86);
				match(T__2);
				setState(87);
				match(T__3);
				setState(88);
				match(T__4);
				setState(89);
				match(T__5);
				setState(90);
				match(T__6);
				setState(91);
				match(T__7);
				setState(92);
				match(T__8);
				setState(93);
				match(T__9);
				setState(94);
				match(IDENTIFIER);
				setState(95);
				match(T__10);
				setState(96);
				match(T__1);
				setState(97);
				statement();
				setState(98);
				match(T__11);
				notifyErrorListeners("Missing ending '}' for the main class");
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(101);
				match(T__0);
				setState(102);
				match(IDENTIFIER);
				setState(103);
				match(T__1);
				setState(104);
				match(T__2);
				setState(105);
				match(T__3);
				setState(106);
				match(T__4);
				setState(107);
				match(T__5);
				setState(108);
				match(T__6);
				setState(109);
				match(T__7);
				setState(110);
				match(T__8);
				setState(111);
				match(T__9);
				setState(112);
				match(IDENTIFIER);
				setState(113);
				match(T__10);
				notifyErrorListeners("Missing '{' at the main method call");
				setState(115);
				statement();
				setState(116);
				match(T__11);
				setState(117);
				match(T__11);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(119);
				match(T__0);
				setState(120);
				match(IDENTIFIER);
				setState(121);
				match(T__1);
				setState(122);
				match(T__2);
				setState(123);
				match(T__3);
				setState(124);
				match(T__4);
				setState(125);
				match(T__5);
				setState(126);
				match(T__6);
				setState(127);
				match(T__7);
				setState(128);
				match(T__8);
				setState(129);
				match(T__9);
				setState(130);
				match(IDENTIFIER);
				setState(131);
				match(T__10);
				setState(132);
				match(T__1);
				setState(133);
				statement();
				notifyErrorListeners("Missing '}' at the end of the main method call");
				setState(135);
				match(T__11);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				notifyErrorListeners("Missing the class identifier");
				setState(138);
				match(IDENTIFIER);
				setState(139);
				match(T__1);
				setState(140);
				match(T__2);
				setState(141);
				match(T__3);
				setState(142);
				match(T__4);
				setState(143);
				match(T__5);
				setState(144);
				match(T__6);
				setState(145);
				match(T__7);
				setState(146);
				match(T__8);
				setState(147);
				match(T__9);
				setState(148);
				match(IDENTIFIER);
				setState(149);
				match(T__10);
				setState(150);
				match(T__1);
				setState(151);
				statement();
				setState(152);
				match(T__11);
				setState(153);
				match(T__11);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDecContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(LavaParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(LavaParser.IDENTIFIER, i);
		}
		public ExtensionContext extension() {
			return getRuleContext(ExtensionContext.class,0);
		}
		public List<FieldDeclarationContext> fieldDeclaration() {
			return getRuleContexts(FieldDeclarationContext.class);
		}
		public FieldDeclarationContext fieldDeclaration(int i) {
			return getRuleContext(FieldDeclarationContext.class,i);
		}
		public List<MethodDecContext> methodDec() {
			return getRuleContexts(MethodDecContext.class);
		}
		public MethodDecContext methodDec(int i) {
			return getRuleContext(MethodDecContext.class,i);
		}
		public ClassDecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterClassDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitClassDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitClassDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassDecContext classDec() throws RecognitionException {
		ClassDecContext _localctx = new ClassDecContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_classDec);
		int _la;
		try {
			int _alt;
			setState(216);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(157);
				match(T__0);
				setState(158);
				match(IDENTIFIER);
				setState(160);
				_la = _input.LA(1);
				if (_la==T__12) {
					{
					setState(159);
					extension();
					}
				}

				setState(162);
				match(T__1);
				setState(166);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(163);
						fieldDeclaration();
						}
						} 
					}
					setState(168);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				}
				setState(172);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__4) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(169);
					methodDec();
					}
					}
					setState(174);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(175);
				match(T__11);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(176);
				match(T__0);
				setState(177);
				match(IDENTIFIER);
				setState(180);
				_la = _input.LA(1);
				if (_la==T__12) {
					{
					setState(178);
					match(T__12);
					setState(179);
					match(IDENTIFIER);
					}
				}

				notifyErrorListeners("Missing '{' at the beginning of the class call");
				setState(186);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(183);
						fieldDeclaration();
						}
						} 
					}
					setState(188);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				}
				setState(192);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__4) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(189);
					methodDec();
					}
					}
					setState(194);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(195);
				match(T__11);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(196);
				match(T__0);
				setState(197);
				match(IDENTIFIER);
				setState(200);
				_la = _input.LA(1);
				if (_la==T__12) {
					{
					setState(198);
					match(T__12);
					setState(199);
					match(IDENTIFIER);
					}
				}

				setState(202);
				match(T__1);
				setState(206);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(203);
						fieldDeclaration();
						}
						} 
					}
					setState(208);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
				}
				setState(212);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__4) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(209);
					methodDec();
					}
					}
					setState(214);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				notifyErrorListeners("Missing '}' at the end of the class definition");
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtensionContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public ExtensionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extension; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterExtension(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitExtension(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitExtension(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExtensionContext extension() throws RecognitionException {
		ExtensionContext _localctx = new ExtensionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_extension);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218);
			match(T__12);
			setState(219);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDecContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public VarDecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterVarDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitVarDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitVarDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarDecContext varDec() throws RecognitionException {
		VarDecContext _localctx = new VarDecContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_varDec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(221);
			type();
			setState(222);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDecContext extends ParserRuleContext {
		public MethodReturnContext methodReturn() {
			return getRuleContext(MethodReturnContext.class,0);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(LavaParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(LavaParser.IDENTIFIER, i);
		}
		public MethodParamsContext methodParams() {
			return getRuleContext(MethodParamsContext.class,0);
		}
		public List<LocalDeclarationContext> localDeclaration() {
			return getRuleContexts(LocalDeclarationContext.class);
		}
		public LocalDeclarationContext localDeclaration(int i) {
			return getRuleContext(LocalDeclarationContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public AReturnContext aReturn() {
			return getRuleContext(AReturnContext.class,0);
		}
		public ReturnTypeContext returnType() {
			return getRuleContext(ReturnTypeContext.class,0);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public MethodDecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodDec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMethodDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMethodDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMethodDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodDecContext methodDec() throws RecognitionException {
		MethodDecContext _localctx = new MethodDecContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_methodDec);
		int _la;
		try {
			int _alt;
			setState(347);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(224);
				match(T__2);
				setState(225);
				methodReturn();
				setState(226);
				match(IDENTIFIER);
				setState(227);
				match(T__6);
				setState(229);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(228);
					methodParams();
					}
				}

				setState(231);
				match(T__10);
				setState(232);
				match(T__1);
				setState(236);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(233);
						localDeclaration();
						}
						} 
					}
					setState(238);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
				}
				setState(242);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(239);
						statement();
						}
						} 
					}
					setState(244);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
				}
				setState(246);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__17) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << INT) | (1L << IDENTIFIER) | (1L << FLOAT))) != 0)) {
					{
					setState(245);
					aReturn();
					}
				}

				setState(248);
				match(T__11);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(250);
				match(T__2);
				setState(251);
				returnType();
				setState(252);
				match(IDENTIFIER);
				setState(253);
				match(T__6);
				setState(255);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(254);
					methodParams();
					}
				}

				setState(257);
				match(T__10);
				notifyErrorListeners("Missing '{' at the beginning of the method call");
				setState(262);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(259);
						localDeclaration();
						}
						} 
					}
					setState(264);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				}
				setState(268);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(265);
						statement();
						}
						} 
					}
					setState(270);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
				}
				setState(272);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__17) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << INT) | (1L << IDENTIFIER) | (1L << FLOAT))) != 0)) {
					{
					setState(271);
					aReturn();
					}
				}

				setState(274);
				match(T__11);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(276);
				match(T__2);
				setState(277);
				returnType();
				setState(278);
				match(IDENTIFIER);
				setState(279);
				match(T__6);
				setState(291);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(280);
					type();
					setState(281);
					match(IDENTIFIER);
					setState(288);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__13) {
						{
						{
						setState(282);
						match(T__13);
						setState(283);
						type();
						setState(284);
						match(IDENTIFIER);
						}
						}
						setState(290);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(293);
				match(T__10);
				setState(294);
				match(T__1);
				setState(298);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(295);
						localDeclaration();
						}
						} 
					}
					setState(300);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				}
				setState(304);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(301);
						statement();
						}
						} 
					}
					setState(306);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
				}
				notifyErrorListeners("Missing '}' at the end of the method call");
				setState(309);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
				case 1:
					{
					setState(308);
					aReturn();
					}
					break;
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				notifyErrorListeners("Missing 'public' declaration for the method call");
				setState(312);
				returnType();
				setState(313);
				match(IDENTIFIER);
				setState(314);
				match(T__6);
				setState(326);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(315);
					type();
					setState(316);
					match(IDENTIFIER);
					setState(323);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__13) {
						{
						{
						setState(317);
						match(T__13);
						setState(318);
						type();
						setState(319);
						match(IDENTIFIER);
						}
						}
						setState(325);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(328);
				match(T__10);
				setState(329);
				match(T__1);
				setState(333);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(330);
						localDeclaration();
						}
						} 
					}
					setState(335);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				}
				setState(339);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(336);
						statement();
						}
						} 
					}
					setState(341);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				}
				setState(343);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__17) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << INT) | (1L << IDENTIFIER) | (1L << FLOAT))) != 0)) {
					{
					setState(342);
					aReturn();
					}
				}

				setState(345);
				match(T__11);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalDeclarationContext extends ParserRuleContext {
		public VarDecContext varDec() {
			return getRuleContext(VarDecContext.class,0);
		}
		public LocalDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterLocalDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitLocalDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitLocalDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocalDeclarationContext localDeclaration() throws RecognitionException {
		LocalDeclarationContext _localctx = new LocalDeclarationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_localDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(349);
			varDec();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldDeclarationContext extends ParserRuleContext {
		public VarDecContext varDec() {
			return getRuleContext(VarDecContext.class,0);
		}
		public FieldDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterFieldDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitFieldDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitFieldDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FieldDeclarationContext fieldDeclaration() throws RecognitionException {
		FieldDeclarationContext _localctx = new FieldDeclarationContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_fieldDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(351);
			varDec();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodReturnContext extends ParserRuleContext {
		public ReturnTypeContext returnType() {
			return getRuleContext(ReturnTypeContext.class,0);
		}
		public MethodReturnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodReturn; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMethodReturn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMethodReturn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMethodReturn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodReturnContext methodReturn() throws RecognitionException {
		MethodReturnContext _localctx = new MethodReturnContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_methodReturn);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(353);
			returnType();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodParamsContext extends ParserRuleContext {
		public List<MethodVarDecContext> methodVarDec() {
			return getRuleContexts(MethodVarDecContext.class);
		}
		public MethodVarDecContext methodVarDec(int i) {
			return getRuleContext(MethodVarDecContext.class,i);
		}
		public MethodParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodParams; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMethodParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMethodParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMethodParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodParamsContext methodParams() throws RecognitionException {
		MethodParamsContext _localctx = new MethodParamsContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_methodParams);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			methodVarDec();
			setState(360);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__13) {
				{
				{
				setState(356);
				match(T__13);
				setState(357);
				methodVarDec();
				}
				}
				setState(362);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodVarDecContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public MethodVarDecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodVarDec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMethodVarDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMethodVarDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMethodVarDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodVarDecContext methodVarDec() throws RecognitionException {
		MethodVarDecContext _localctx = new MethodVarDecContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_methodVarDec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(363);
			type();
			setState(364);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_type);
		try {
			setState(373);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(366);
				match(T__14);
				setState(367);
				match(T__8);
				setState(368);
				match(T__9);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(369);
				match(T__15);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(370);
				match(T__14);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(371);
				match(T__16);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(372);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnTypeContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ReturnTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterReturnType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitReturnType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitReturnType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnTypeContext returnType() throws RecognitionException {
		ReturnTypeContext _localctx = new ReturnTypeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_returnType);
		try {
			setState(377);
			switch (_input.LA(1)) {
			case T__14:
			case T__15:
			case T__16:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(375);
				type();
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				setState(376);
				match(T__4);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AReturnContext extends ParserRuleContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public AReturnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aReturn; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterAReturn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitAReturn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitAReturn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AReturnContext aReturn() throws RecognitionException {
		AReturnContext _localctx = new AReturnContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_aReturn);
		try {
			setState(385);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(379);
				match(T__17);
				setState(380);
				exp(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				notifyErrorListeners("Missing a return key!");
				setState(382);
				exp(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(383);
				match(T__17);
				notifyErrorListeners("Missing something to return!");
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class WhileStatementContext extends StatementContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public WBlockContext wBlock() {
			return getRuleContext(WBlockContext.class,0);
		}
		public WhileStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitWhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitWhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BlockStatementContext extends StatementContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterBlockStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitBlockStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitBlockStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConsolePrintContext extends StatementContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ConsolePrintContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterConsolePrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitConsolePrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitConsolePrint(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfErrorContext extends StatementContext {
		public IfErrorContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterIfError(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitIfError(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitIfError(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AssignmentStatementContext extends StatementContext {
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public AssignmentStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterAssignmentStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitAssignmentStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitAssignmentStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfElseStatementContext extends StatementContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public IfBlockContext ifBlock() {
			return getRuleContext(IfBlockContext.class,0);
		}
		public EBlockContext eBlock() {
			return getRuleContext(EBlockContext.class,0);
		}
		public IfElseStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterIfElseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitIfElseStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitIfElseStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ErrorWhileContext extends StatementContext {
		public WBlockContext wBlock() {
			return getRuleContext(WBlockContext.class,0);
		}
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ErrorWhileContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterErrorWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitErrorWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitErrorWhile(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ErrorConsoleContext extends StatementContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ErrorConsoleContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterErrorConsole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitErrorConsole(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitErrorConsole(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayAssignmentStatementContext extends StatementContext {
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public ArrayAssignmentStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterArrayAssignmentStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitArrayAssignmentStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitArrayAssignmentStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_statement);
		int _la;
		try {
			setState(453);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				_localctx = new BlockStatementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(387);
				match(T__1);
				setState(391);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(388);
					statement();
					}
					}
					setState(393);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(394);
				match(T__11);
				}
				break;
			case 2:
				_localctx = new IfElseStatementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(395);
				match(T__18);
				setState(396);
				match(T__6);
				setState(397);
				exp(0);
				setState(398);
				match(T__10);
				setState(399);
				ifBlock();
				setState(400);
				match(T__19);
				setState(401);
				eBlock();
				}
				break;
			case 3:
				_localctx = new IfErrorContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(403);
				match(T__19);
				notifyErrorListeners("There is just some random else for no reason!");
				}
				break;
			case 4:
				_localctx = new WhileStatementContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(405);
				match(T__20);
				setState(406);
				match(T__6);
				setState(407);
				exp(0);
				setState(408);
				match(T__10);
				setState(409);
				wBlock();
				}
				break;
			case 5:
				_localctx = new ErrorWhileContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(411);
				match(T__20);
				setState(412);
				match(T__6);
				notifyErrorListeners("There is no expression to evaluate in this while loop!");
				setState(414);
				match(T__10);
				setState(415);
				wBlock();
				}
				break;
			case 6:
				_localctx = new ErrorWhileContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(416);
				match(T__20);
				notifyErrorListeners("Missing a '(' in the 'while' loop");
				setState(418);
				exp(0);
				setState(419);
				match(T__10);
				setState(420);
				wBlock();
				}
				break;
			case 7:
				_localctx = new ErrorWhileContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(422);
				match(T__20);
				setState(423);
				match(T__6);
				setState(424);
				exp(0);
				notifyErrorListeners("Missing a ')' in the 'while' loop");
				setState(426);
				wBlock();
				}
				break;
			case 8:
				_localctx = new ConsolePrintContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(428);
				match(T__21);
				setState(429);
				match(T__6);
				setState(430);
				exp(0);
				setState(431);
				match(T__10);
				}
				break;
			case 9:
				_localctx = new ErrorConsoleContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(433);
				match(T__21);
				notifyErrorListeners("Missing a '(' in the System.out.println!");
				setState(435);
				exp(0);
				setState(436);
				match(T__10);
				}
				break;
			case 10:
				_localctx = new ErrorConsoleContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(438);
				match(T__21);
				setState(439);
				match(T__6);
				setState(440);
				exp(0);
				notifyErrorListeners("Missing a ')' in the System.out.println!");
				}
				break;
			case 11:
				_localctx = new AssignmentStatementContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(443);
				match(IDENTIFIER);
				setState(444);
				match(T__22);
				setState(445);
				exp(0);
				}
				break;
			case 12:
				_localctx = new ArrayAssignmentStatementContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(446);
				match(IDENTIFIER);
				setState(447);
				match(T__8);
				setState(448);
				exp(0);
				setState(449);
				match(T__9);
				setState(450);
				match(T__22);
				setState(451);
				exp(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfBlockContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public IfBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterIfBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitIfBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitIfBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfBlockContext ifBlock() throws RecognitionException {
		IfBlockContext _localctx = new IfBlockContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_ifBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(455);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EBlockContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public EBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterEBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitEBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitEBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EBlockContext eBlock() throws RecognitionException {
		EBlockContext _localctx = new EBlockContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_eBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(457);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WBlockContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public WBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterWBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitWBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitWBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WBlockContext wBlock() throws RecognitionException {
		WBlockContext _localctx = new WBlockContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_wBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(459);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	 
		public ExpContext() { }
		public void copyFrom(ExpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BooleanTrueExpressionContext extends ExpContext {
		public BooleanTrueExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterBooleanTrueExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitBooleanTrueExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitBooleanTrueExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjectInstantiationExpressionContext extends ExpContext {
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public ObjectInstantiationExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterObjectInstantiationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitObjectInstantiationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitObjectInstantiationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntLiteralExpressionContext extends ExpContext {
		public TerminalNode INT() { return getToken(LavaParser.INT, 0); }
		public IntLiteralExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterIntLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitIntLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitIntLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MethodCallExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public MethodCallExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMethodCallExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMethodCallExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMethodCallExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotExpressionContext extends ExpContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public NotExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterNotExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitNotExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitNotExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiplyExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public MultiplyExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterMultiplyExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitMultiplyExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitMultiplyExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GreaterThanExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public GreaterThanExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterGreaterThanExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitGreaterThanExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitGreaterThanExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BracketExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public BracketExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterBracketExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitBracketExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitBracketExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExperssionContext extends ExpContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ParenExperssionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterParenExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitParenExperssion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitParenExperssion(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LengthIdentifierContext extends ExpContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public LengthIdentifierContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterLengthIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitLengthIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitLengthIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassAssignmentContext extends ExpContext {
		public ClassAssignmentContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterClassAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitClassAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitClassAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public AndExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener) ((LavaListener)listener).enterAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor) return ((LavaVisitor<? extends T>)visitor).visitAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AddExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public AddExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterAddExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitAddExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitAddExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayInitializerExpressionContext extends ExpContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ArrayInitializerExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterArrayInitializerExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitArrayInitializerExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitArrayInitializerExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubtractionExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public SubtractionExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterSubtractionExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitSubtractionExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitSubtractionExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LessThanExpressionContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public LessThanExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterLessThanExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitLessThanExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitLessThanExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BooleanFalseExpressionContext extends ExpContext {
		public BooleanFalseExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterBooleanFalseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitBooleanFalseExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitBooleanFalseExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LiteralIdentifierExpressionContext extends ExpContext {
		public TerminalNode IDENTIFIER() { return getToken(LavaParser.IDENTIFIER, 0); }
		public LiteralIdentifierExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterLiteralIdentifierExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitLiteralIdentifierExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitLiteralIdentifierExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FloatExpressionContext extends ExpContext {
		public TerminalNode FLOAT() { return getToken(LavaParser.FLOAT, 0); }
		public FloatExpressionContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).enterFloatExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LavaListener ) ((LavaListener)listener).exitFloatExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LavaVisitor ) return ((LavaVisitor<? extends T>)visitor).visitFloatExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpContext exp() throws RecognitionException {
		return exp(0);
	}

	private ExpContext exp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpContext _localctx = new ExpContext(_ctx, _parentState);
		ExpContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_exp, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(484);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				{
				_localctx = new IntLiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(462);
				match(INT);
				}
				break;
			case 2:
				{
				_localctx = new BooleanTrueExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(463);
				match(T__31);
				}
				break;
			case 3:
				{
				_localctx = new BooleanFalseExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(464);
				match(T__32);
				}
				break;
			case 4:
				{
				_localctx = new LiteralIdentifierExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(465);
				match(IDENTIFIER);
				}
				break;
			case 5:
				{
				_localctx = new FloatExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(466);
				match(FLOAT);
				}
				break;
			case 6:
				{
				_localctx = new ClassAssignmentContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(467);
				match(T__33);
				}
				break;
			case 7:
				{
				_localctx = new ArrayInitializerExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(468);
				match(T__34);
				setState(469);
				match(T__14);
				setState(470);
				match(T__8);
				setState(471);
				exp(0);
				setState(472);
				match(T__9);
				}
				break;
			case 8:
				{
				_localctx = new ObjectInstantiationExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(474);
				match(T__34);
				setState(475);
				match(IDENTIFIER);
				setState(476);
				match(T__6);
				setState(477);
				match(T__10);
				}
				break;
			case 9:
				{
				_localctx = new NotExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(478);
				match(T__35);
				setState(479);
				exp(2);
				}
				break;
			case 10:
				{
				_localctx = new ParenExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(480);
				match(T__6);
				setState(481);
				exp(0);
				setState(482);
				match(T__10);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(529);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(527);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
					case 1:
						{
						_localctx = new AddExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(486);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(487);
						match(T__23);
						setState(488);
						exp(20);
						}
						break;
					case 2:
						{
						_localctx = new SubtractionExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(489);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(490);
						match(T__24);
						setState(491);
						exp(19);
						}
						break;
					case 3:
						{
						_localctx = new MultiplyExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(492);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(493);
						match(T__25);
						setState(494);
						exp(18);
						}
						break;
					case 4:
						{
						_localctx = new AndExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(495);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(496);
						match(T__26);
						setState(497);
						exp(17);
						}
						break;
					case 5:
						{
						_localctx = new LessThanExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(498);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(499);
						match(T__27);
						setState(500);
						exp(16);
						}
						break;
					case 6:
						{
						_localctx = new GreaterThanExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(501);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(502);
						match(T__28);
						setState(503);
						exp(15);
						}
						break;
					case 7:
						{
						_localctx = new BracketExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(504);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(505);
						match(T__8);
						setState(506);
						exp(0);
						setState(507);
						match(T__9);
						}
						break;
					case 8:
						{
						_localctx = new LengthIdentifierContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(509);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(510);
						match(T__29);
						setState(511);
						match(T__30);
						}
						break;
					case 9:
						{
						_localctx = new MethodCallExpressionContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(512);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(513);
						match(T__29);
						setState(514);
						match(IDENTIFIER);
						setState(515);
						match(T__6);
						setState(524);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << INT) | (1L << IDENTIFIER) | (1L << FLOAT))) != 0)) {
							{
							setState(516);
							exp(0);
							setState(521);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==T__13) {
								{
								{
								setState(517);
								match(T__13);
								setState(518);
								exp(0);
								}
								}
								setState(523);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(526);
						match(T__10);
						}
						break;
					}
					} 
				}
				setState(531);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 18:
			return exp_sempred((ExpContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean exp_sempred(ExpContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 19);
		case 1:
			return precpred(_ctx, 18);
		case 2:
			return precpred(_ctx, 17);
		case 3:
			return precpred(_ctx, 16);
		case 4:
			return precpred(_ctx, 15);
		case 5:
			return precpred(_ctx, 14);
		case 6:
			return precpred(_ctx, 13);
		case 7:
			return precpred(_ctx, 12);
		case 8:
			return precpred(_ctx, 11);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3.\u0217\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\3\2\3\2\7\2+\n\2\f\2\16\2.\13\2\3\2\3\2\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\5\3\u009e\n\3\3\4\3\4\3\4\5\4\u00a3\n\4\3\4\3\4\7\4\u00a7\n\4\f\4\16"+
		"\4\u00aa\13\4\3\4\7\4\u00ad\n\4\f\4\16\4\u00b0\13\4\3\4\3\4\3\4\3\4\3"+
		"\4\5\4\u00b7\n\4\3\4\3\4\7\4\u00bb\n\4\f\4\16\4\u00be\13\4\3\4\7\4\u00c1"+
		"\n\4\f\4\16\4\u00c4\13\4\3\4\3\4\3\4\3\4\3\4\5\4\u00cb\n\4\3\4\3\4\7\4"+
		"\u00cf\n\4\f\4\16\4\u00d2\13\4\3\4\7\4\u00d5\n\4\f\4\16\4\u00d8\13\4\3"+
		"\4\5\4\u00db\n\4\3\5\3\5\3\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\5\7\u00e8"+
		"\n\7\3\7\3\7\3\7\7\7\u00ed\n\7\f\7\16\7\u00f0\13\7\3\7\7\7\u00f3\n\7\f"+
		"\7\16\7\u00f6\13\7\3\7\5\7\u00f9\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0102"+
		"\n\7\3\7\3\7\3\7\7\7\u0107\n\7\f\7\16\7\u010a\13\7\3\7\7\7\u010d\n\7\f"+
		"\7\16\7\u0110\13\7\3\7\5\7\u0113\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\7\7\u0121\n\7\f\7\16\7\u0124\13\7\5\7\u0126\n\7\3\7\3\7"+
		"\3\7\7\7\u012b\n\7\f\7\16\7\u012e\13\7\3\7\7\7\u0131\n\7\f\7\16\7\u0134"+
		"\13\7\3\7\3\7\5\7\u0138\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7"+
		"\7\u0144\n\7\f\7\16\7\u0147\13\7\5\7\u0149\n\7\3\7\3\7\3\7\7\7\u014e\n"+
		"\7\f\7\16\7\u0151\13\7\3\7\7\7\u0154\n\7\f\7\16\7\u0157\13\7\3\7\5\7\u015a"+
		"\n\7\3\7\3\7\5\7\u015e\n\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\7\13"+
		"\u0169\n\13\f\13\16\13\u016c\13\13\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\5\r\u0178\n\r\3\16\3\16\5\16\u017c\n\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\5\17\u0184\n\17\3\20\3\20\7\20\u0188\n\20\f\20\16\20\u018b\13\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\5\20\u01c8\n\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u01e7\n\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\7\24\u020a\n\24\f\24\16\24\u020d\13\24\5\24\u020f\n\24\3\24\7\24"+
		"\u0212\n\24\f\24\16\24\u0215\13\24\3\24\2\3&\25\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&\2\2\u0251\2(\3\2\2\2\4\u009d\3\2\2\2\6\u00da\3\2"+
		"\2\2\b\u00dc\3\2\2\2\n\u00df\3\2\2\2\f\u015d\3\2\2\2\16\u015f\3\2\2\2"+
		"\20\u0161\3\2\2\2\22\u0163\3\2\2\2\24\u0165\3\2\2\2\26\u016d\3\2\2\2\30"+
		"\u0177\3\2\2\2\32\u017b\3\2\2\2\34\u0183\3\2\2\2\36\u01c7\3\2\2\2 \u01c9"+
		"\3\2\2\2\"\u01cb\3\2\2\2$\u01cd\3\2\2\2&\u01e6\3\2\2\2(,\5\4\3\2)+\5\6"+
		"\4\2*)\3\2\2\2+.\3\2\2\2,*\3\2\2\2,-\3\2\2\2-/\3\2\2\2.,\3\2\2\2/\60\7"+
		"\2\2\3\60\3\3\2\2\2\61\62\7\3\2\2\62\63\7(\2\2\63\64\7\4\2\2\64\65\7\5"+
		"\2\2\65\66\7\6\2\2\66\67\7\7\2\2\678\7\b\2\289\7\t\2\29:\7\n\2\2:;\7\13"+
		"\2\2;<\7\f\2\2<=\7(\2\2=>\7\r\2\2>?\7\4\2\2?@\5\36\20\2@A\7\16\2\2AB\7"+
		"\16\2\2B\u009e\3\2\2\2CD\7\3\2\2DE\7(\2\2EF\b\3\1\2FG\7\5\2\2GH\7\6\2"+
		"\2HI\7\7\2\2IJ\7\b\2\2JK\7\t\2\2KL\7\n\2\2LM\7\13\2\2MN\7\f\2\2NO\7(\2"+
		"\2OP\7\r\2\2PQ\7\4\2\2QR\5\36\20\2RS\7\16\2\2ST\7\16\2\2T\u009e\3\2\2"+
		"\2UV\7\3\2\2VW\7(\2\2WX\7\4\2\2XY\7\5\2\2YZ\7\6\2\2Z[\7\7\2\2[\\\7\b\2"+
		"\2\\]\7\t\2\2]^\7\n\2\2^_\7\13\2\2_`\7\f\2\2`a\7(\2\2ab\7\r\2\2bc\7\4"+
		"\2\2cd\5\36\20\2de\7\16\2\2ef\b\3\1\2f\u009e\3\2\2\2gh\7\3\2\2hi\7(\2"+
		"\2ij\7\4\2\2jk\7\5\2\2kl\7\6\2\2lm\7\7\2\2mn\7\b\2\2no\7\t\2\2op\7\n\2"+
		"\2pq\7\13\2\2qr\7\f\2\2rs\7(\2\2st\7\r\2\2tu\b\3\1\2uv\5\36\20\2vw\7\16"+
		"\2\2wx\7\16\2\2x\u009e\3\2\2\2yz\7\3\2\2z{\7(\2\2{|\7\4\2\2|}\7\5\2\2"+
		"}~\7\6\2\2~\177\7\7\2\2\177\u0080\7\b\2\2\u0080\u0081\7\t\2\2\u0081\u0082"+
		"\7\n\2\2\u0082\u0083\7\13\2\2\u0083\u0084\7\f\2\2\u0084\u0085\7(\2\2\u0085"+
		"\u0086\7\r\2\2\u0086\u0087\7\4\2\2\u0087\u0088\5\36\20\2\u0088\u0089\b"+
		"\3\1\2\u0089\u008a\7\16\2\2\u008a\u009e\3\2\2\2\u008b\u008c\b\3\1\2\u008c"+
		"\u008d\7(\2\2\u008d\u008e\7\4\2\2\u008e\u008f\7\5\2\2\u008f\u0090\7\6"+
		"\2\2\u0090\u0091\7\7\2\2\u0091\u0092\7\b\2\2\u0092\u0093\7\t\2\2\u0093"+
		"\u0094\7\n\2\2\u0094\u0095\7\13\2\2\u0095\u0096\7\f\2\2\u0096\u0097\7"+
		"(\2\2\u0097\u0098\7\r\2\2\u0098\u0099\7\4\2\2\u0099\u009a\5\36\20\2\u009a"+
		"\u009b\7\16\2\2\u009b\u009c\7\16\2\2\u009c\u009e\3\2\2\2\u009d\61\3\2"+
		"\2\2\u009dC\3\2\2\2\u009dU\3\2\2\2\u009dg\3\2\2\2\u009dy\3\2\2\2\u009d"+
		"\u008b\3\2\2\2\u009e\5\3\2\2\2\u009f\u00a0\7\3\2\2\u00a0\u00a2\7(\2\2"+
		"\u00a1\u00a3\5\b\5\2\u00a2\u00a1\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4"+
		"\3\2\2\2\u00a4\u00a8\7\4\2\2\u00a5\u00a7\5\20\t\2\u00a6\u00a5\3\2\2\2"+
		"\u00a7\u00aa\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00ae"+
		"\3\2\2\2\u00aa\u00a8\3\2\2\2\u00ab\u00ad\5\f\7\2\u00ac\u00ab\3\2\2\2\u00ad"+
		"\u00b0\3\2\2\2\u00ae\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b1\3\2"+
		"\2\2\u00b0\u00ae\3\2\2\2\u00b1\u00db\7\16\2\2\u00b2\u00b3\7\3\2\2\u00b3"+
		"\u00b6\7(\2\2\u00b4\u00b5\7\17\2\2\u00b5\u00b7\7(\2\2\u00b6\u00b4\3\2"+
		"\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00bc\b\4\1\2\u00b9"+
		"\u00bb\5\20\t\2\u00ba\u00b9\3\2\2\2\u00bb\u00be\3\2\2\2\u00bc\u00ba\3"+
		"\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00c2\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf"+
		"\u00c1\5\f\7\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4\3\2\2\2\u00c2\u00c0\3\2"+
		"\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c5\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c5"+
		"\u00db\7\16\2\2\u00c6\u00c7\7\3\2\2\u00c7\u00ca\7(\2\2\u00c8\u00c9\7\17"+
		"\2\2\u00c9\u00cb\7(\2\2\u00ca\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb"+
		"\u00cc\3\2\2\2\u00cc\u00d0\7\4\2\2\u00cd\u00cf\5\20\t\2\u00ce\u00cd\3"+
		"\2\2\2\u00cf\u00d2\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1"+
		"\u00d6\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d3\u00d5\5\f\7\2\u00d4\u00d3\3\2"+
		"\2\2\u00d5\u00d8\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7"+
		"\u00d9\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d9\u00db\b\4\1\2\u00da\u009f\3\2"+
		"\2\2\u00da\u00b2\3\2\2\2\u00da\u00c6\3\2\2\2\u00db\7\3\2\2\2\u00dc\u00dd"+
		"\7\17\2\2\u00dd\u00de\7(\2\2\u00de\t\3\2\2\2\u00df\u00e0\5\30\r\2\u00e0"+
		"\u00e1\7(\2\2\u00e1\13\3\2\2\2\u00e2\u00e3\7\5\2\2\u00e3\u00e4\5\22\n"+
		"\2\u00e4\u00e5\7(\2\2\u00e5\u00e7\7\t\2\2\u00e6\u00e8\5\24\13\2\u00e7"+
		"\u00e6\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ea\7\r"+
		"\2\2\u00ea\u00ee\7\4\2\2\u00eb\u00ed\5\16\b\2\u00ec\u00eb\3\2\2\2\u00ed"+
		"\u00f0\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f4\3\2"+
		"\2\2\u00f0\u00ee\3\2\2\2\u00f1\u00f3\5\36\20\2\u00f2\u00f1\3\2\2\2\u00f3"+
		"\u00f6\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f8\3\2"+
		"\2\2\u00f6\u00f4\3\2\2\2\u00f7\u00f9\5\34\17\2\u00f8\u00f7\3\2\2\2\u00f8"+
		"\u00f9\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa\u00fb\7\16\2\2\u00fb\u015e\3"+
		"\2\2\2\u00fc\u00fd\7\5\2\2\u00fd\u00fe\5\32\16\2\u00fe\u00ff\7(\2\2\u00ff"+
		"\u0101\7\t\2\2\u0100\u0102\5\24\13\2\u0101\u0100\3\2\2\2\u0101\u0102\3"+
		"\2\2\2\u0102\u0103\3\2\2\2\u0103\u0104\7\r\2\2\u0104\u0108\b\7\1\2\u0105"+
		"\u0107\5\16\b\2\u0106\u0105\3\2\2\2\u0107\u010a\3\2\2\2\u0108\u0106\3"+
		"\2\2\2\u0108\u0109\3\2\2\2\u0109\u010e\3\2\2\2\u010a\u0108\3\2\2\2\u010b"+
		"\u010d\5\36\20\2\u010c\u010b\3\2\2\2\u010d\u0110\3\2\2\2\u010e\u010c\3"+
		"\2\2\2\u010e\u010f\3\2\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0111"+
		"\u0113\5\34\17\2\u0112\u0111\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0114\3"+
		"\2\2\2\u0114\u0115\7\16\2\2\u0115\u015e\3\2\2\2\u0116\u0117\7\5\2\2\u0117"+
		"\u0118\5\32\16\2\u0118\u0119\7(\2\2\u0119\u0125\7\t\2\2\u011a\u011b\5"+
		"\30\r\2\u011b\u0122\7(\2\2\u011c\u011d\7\20\2\2\u011d\u011e\5\30\r\2\u011e"+
		"\u011f\7(\2\2\u011f\u0121\3\2\2\2\u0120\u011c\3\2\2\2\u0121\u0124\3\2"+
		"\2\2\u0122\u0120\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u0126\3\2\2\2\u0124"+
		"\u0122\3\2\2\2\u0125\u011a\3\2\2\2\u0125\u0126\3\2\2\2\u0126\u0127\3\2"+
		"\2\2\u0127\u0128\7\r\2\2\u0128\u012c\7\4\2\2\u0129\u012b\5\16\b\2\u012a"+
		"\u0129\3\2\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3\2\2\2\u012c\u012d\3\2"+
		"\2\2\u012d\u0132\3\2\2\2\u012e\u012c\3\2\2\2\u012f\u0131\5\36\20\2\u0130"+
		"\u012f\3\2\2\2\u0131\u0134\3\2\2\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2"+
		"\2\2\u0133\u0135\3\2\2\2\u0134\u0132\3\2\2\2\u0135\u0137\b\7\1\2\u0136"+
		"\u0138\5\34\17\2\u0137\u0136\3\2\2\2\u0137\u0138\3\2\2\2\u0138\u015e\3"+
		"\2\2\2\u0139\u013a\b\7\1\2\u013a\u013b\5\32\16\2\u013b\u013c\7(\2\2\u013c"+
		"\u0148\7\t\2\2\u013d\u013e\5\30\r\2\u013e\u0145\7(\2\2\u013f\u0140\7\20"+
		"\2\2\u0140\u0141\5\30\r\2\u0141\u0142\7(\2\2\u0142\u0144\3\2\2\2\u0143"+
		"\u013f\3\2\2\2\u0144\u0147\3\2\2\2\u0145\u0143\3\2\2\2\u0145\u0146\3\2"+
		"\2\2\u0146\u0149\3\2\2\2\u0147\u0145\3\2\2\2\u0148\u013d\3\2\2\2\u0148"+
		"\u0149\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u014b\7\r\2\2\u014b\u014f\7\4"+
		"\2\2\u014c\u014e\5\16\b\2\u014d\u014c\3\2\2\2\u014e\u0151\3\2\2\2\u014f"+
		"\u014d\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0155\3\2\2\2\u0151\u014f\3\2"+
		"\2\2\u0152\u0154\5\36\20\2\u0153\u0152\3\2\2\2\u0154\u0157\3\2\2\2\u0155"+
		"\u0153\3\2\2\2\u0155\u0156\3\2\2\2\u0156\u0159\3\2\2\2\u0157\u0155\3\2"+
		"\2\2\u0158\u015a\5\34\17\2\u0159\u0158\3\2\2\2\u0159\u015a\3\2\2\2\u015a"+
		"\u015b\3\2\2\2\u015b\u015c\7\16\2\2\u015c\u015e\3\2\2\2\u015d\u00e2\3"+
		"\2\2\2\u015d\u00fc\3\2\2\2\u015d\u0116\3\2\2\2\u015d\u0139\3\2\2\2\u015e"+
		"\r\3\2\2\2\u015f\u0160\5\n\6\2\u0160\17\3\2\2\2\u0161\u0162\5\n\6\2\u0162"+
		"\21\3\2\2\2\u0163\u0164\5\32\16\2\u0164\23\3\2\2\2\u0165\u016a\5\26\f"+
		"\2\u0166\u0167\7\20\2\2\u0167\u0169\5\26\f\2\u0168\u0166\3\2\2\2\u0169"+
		"\u016c\3\2\2\2\u016a\u0168\3\2\2\2\u016a\u016b\3\2\2\2\u016b\25\3\2\2"+
		"\2\u016c\u016a\3\2\2\2\u016d\u016e\5\30\r\2\u016e\u016f\7(\2\2\u016f\27"+
		"\3\2\2\2\u0170\u0171\7\21\2\2\u0171\u0172\7\13\2\2\u0172\u0178\7\f\2\2"+
		"\u0173\u0178\7\22\2\2\u0174\u0178\7\21\2\2\u0175\u0178\7\23\2\2\u0176"+
		"\u0178\7(\2\2\u0177\u0170\3\2\2\2\u0177\u0173\3\2\2\2\u0177\u0174\3\2"+
		"\2\2\u0177\u0175\3\2\2\2\u0177\u0176\3\2\2\2\u0178\31\3\2\2\2\u0179\u017c"+
		"\5\30\r\2\u017a\u017c\7\7\2\2\u017b\u0179\3\2\2\2\u017b\u017a\3\2\2\2"+
		"\u017c\33\3\2\2\2\u017d\u017e\7\24\2\2\u017e\u0184\5&\24\2\u017f\u0180"+
		"\b\17\1\2\u0180\u0184\5&\24\2\u0181\u0182\7\24\2\2\u0182\u0184\b\17\1"+
		"\2\u0183\u017d\3\2\2\2\u0183\u017f\3\2\2\2\u0183\u0181\3\2\2\2\u0184\35"+
		"\3\2\2\2\u0185\u0189\7\4\2\2\u0186\u0188\5\36\20\2\u0187\u0186\3\2\2\2"+
		"\u0188\u018b\3\2\2\2\u0189\u0187\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018c"+
		"\3\2\2\2\u018b\u0189\3\2\2\2\u018c\u01c8\7\16\2\2\u018d\u018e\7\25\2\2"+
		"\u018e\u018f\7\t\2\2\u018f\u0190\5&\24\2\u0190\u0191\7\r\2\2\u0191\u0192"+
		"\5 \21\2\u0192\u0193\7\26\2\2\u0193\u0194\5\"\22\2\u0194\u01c8\3\2\2\2"+
		"\u0195\u0196\7\26\2\2\u0196\u01c8\b\20\1\2\u0197\u0198\7\27\2\2\u0198"+
		"\u0199\7\t\2\2\u0199\u019a\5&\24\2\u019a\u019b\7\r\2\2\u019b\u019c\5$"+
		"\23\2\u019c\u01c8\3\2\2\2\u019d\u019e\7\27\2\2\u019e\u019f\7\t\2\2\u019f"+
		"\u01a0\b\20\1\2\u01a0\u01a1\7\r\2\2\u01a1\u01c8\5$\23\2\u01a2\u01a3\7"+
		"\27\2\2\u01a3\u01a4\b\20\1\2\u01a4\u01a5\5&\24\2\u01a5\u01a6\7\r\2\2\u01a6"+
		"\u01a7\5$\23\2\u01a7\u01c8\3\2\2\2\u01a8\u01a9\7\27\2\2\u01a9\u01aa\7"+
		"\t\2\2\u01aa\u01ab\5&\24\2\u01ab\u01ac\b\20\1\2\u01ac\u01ad\5$\23\2\u01ad"+
		"\u01c8\3\2\2\2\u01ae\u01af\7\30\2\2\u01af\u01b0\7\t\2\2\u01b0\u01b1\5"+
		"&\24\2\u01b1\u01b2\7\r\2\2\u01b2\u01c8\3\2\2\2\u01b3\u01b4\7\30\2\2\u01b4"+
		"\u01b5\b\20\1\2\u01b5\u01b6\5&\24\2\u01b6\u01b7\7\r\2\2\u01b7\u01c8\3"+
		"\2\2\2\u01b8\u01b9\7\30\2\2\u01b9\u01ba\7\t\2\2\u01ba\u01bb\5&\24\2\u01bb"+
		"\u01bc\b\20\1\2\u01bc\u01c8\3\2\2\2\u01bd\u01be\7(\2\2\u01be\u01bf\7\31"+
		"\2\2\u01bf\u01c8\5&\24\2\u01c0\u01c1\7(\2\2\u01c1\u01c2\7\13\2\2\u01c2"+
		"\u01c3\5&\24\2\u01c3\u01c4\7\f\2\2\u01c4\u01c5\7\31\2\2\u01c5\u01c6\5"+
		"&\24\2\u01c6\u01c8\3\2\2\2\u01c7\u0185\3\2\2\2\u01c7\u018d\3\2\2\2\u01c7"+
		"\u0195\3\2\2\2\u01c7\u0197\3\2\2\2\u01c7\u019d\3\2\2\2\u01c7\u01a2\3\2"+
		"\2\2\u01c7\u01a8\3\2\2\2\u01c7\u01ae\3\2\2\2\u01c7\u01b3\3\2\2\2\u01c7"+
		"\u01b8\3\2\2\2\u01c7\u01bd\3\2\2\2\u01c7\u01c0\3\2\2\2\u01c8\37\3\2\2"+
		"\2\u01c9\u01ca\5\36\20\2\u01ca!\3\2\2\2\u01cb\u01cc\5\36\20\2\u01cc#\3"+
		"\2\2\2\u01cd\u01ce\5\36\20\2\u01ce%\3\2\2\2\u01cf\u01d0\b\24\1\2\u01d0"+
		"\u01e7\7\'\2\2\u01d1\u01e7\7\"\2\2\u01d2\u01e7\7#\2\2\u01d3\u01e7\7(\2"+
		"\2\u01d4\u01e7\7+\2\2\u01d5\u01e7\7$\2\2\u01d6\u01d7\7%\2\2\u01d7\u01d8"+
		"\7\21\2\2\u01d8\u01d9\7\13\2\2\u01d9\u01da\5&\24\2\u01da\u01db\7\f\2\2"+
		"\u01db\u01e7\3\2\2\2\u01dc\u01dd\7%\2\2\u01dd\u01de\7(\2\2\u01de\u01df"+
		"\7\t\2\2\u01df\u01e7\7\r\2\2\u01e0\u01e1\7&\2\2\u01e1\u01e7\5&\24\4\u01e2"+
		"\u01e3\7\t\2\2\u01e3\u01e4\5&\24\2\u01e4\u01e5\7\r\2\2\u01e5\u01e7\3\2"+
		"\2\2\u01e6\u01cf\3\2\2\2\u01e6\u01d1\3\2\2\2\u01e6\u01d2\3\2\2\2\u01e6"+
		"\u01d3\3\2\2\2\u01e6\u01d4\3\2\2\2\u01e6\u01d5\3\2\2\2\u01e6\u01d6\3\2"+
		"\2\2\u01e6\u01dc\3\2\2\2\u01e6\u01e0\3\2\2\2\u01e6\u01e2\3\2\2\2\u01e7"+
		"\u0213\3\2\2\2\u01e8\u01e9\f\25\2\2\u01e9\u01ea\7\32\2\2\u01ea\u0212\5"+
		"&\24\26\u01eb\u01ec\f\24\2\2\u01ec\u01ed\7\33\2\2\u01ed\u0212\5&\24\25"+
		"\u01ee\u01ef\f\23\2\2\u01ef\u01f0\7\34\2\2\u01f0\u0212\5&\24\24\u01f1"+
		"\u01f2\f\22\2\2\u01f2\u01f3\7\35\2\2\u01f3\u0212\5&\24\23\u01f4\u01f5"+
		"\f\21\2\2\u01f5\u01f6\7\36\2\2\u01f6\u0212\5&\24\22\u01f7\u01f8\f\20\2"+
		"\2\u01f8\u01f9\7\37\2\2\u01f9\u0212\5&\24\21\u01fa\u01fb\f\17\2\2\u01fb"+
		"\u01fc\7\13\2\2\u01fc\u01fd\5&\24\2\u01fd\u01fe\7\f\2\2\u01fe\u0212\3"+
		"\2\2\2\u01ff\u0200\f\16\2\2\u0200\u0201\7 \2\2\u0201\u0212\7!\2\2\u0202"+
		"\u0203\f\r\2\2\u0203\u0204\7 \2\2\u0204\u0205\7(\2\2\u0205\u020e\7\t\2"+
		"\2\u0206\u020b\5&\24\2\u0207\u0208\7\20\2\2\u0208\u020a\5&\24\2\u0209"+
		"\u0207\3\2\2\2\u020a\u020d\3\2\2\2\u020b\u0209\3\2\2\2\u020b\u020c\3\2"+
		"\2\2\u020c\u020f\3\2\2\2\u020d\u020b\3\2\2\2\u020e\u0206\3\2\2\2\u020e"+
		"\u020f\3\2\2\2\u020f\u0210\3\2\2\2\u0210\u0212\7\r\2\2\u0211\u01e8\3\2"+
		"\2\2\u0211\u01eb\3\2\2\2\u0211\u01ee\3\2\2\2\u0211\u01f1\3\2\2\2\u0211"+
		"\u01f4\3\2\2\2\u0211\u01f7\3\2\2\2\u0211\u01fa\3\2\2\2\u0211\u01ff\3\2"+
		"\2\2\u0211\u0202\3\2\2\2\u0212\u0215\3\2\2\2\u0213\u0211\3\2\2\2\u0213"+
		"\u0214\3\2\2\2\u0214\'\3\2\2\2\u0215\u0213\3\2\2\2,,\u009d\u00a2\u00a8"+
		"\u00ae\u00b6\u00bc\u00c2\u00ca\u00d0\u00d6\u00da\u00e7\u00ee\u00f4\u00f8"+
		"\u0101\u0108\u010e\u0112\u0122\u0125\u012c\u0132\u0137\u0145\u0148\u014f"+
		"\u0155\u0159\u015d\u016a\u0177\u017b\u0183\u0189\u01c7\u01e6\u020b\u020e"+
		"\u0211\u0213";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}