package main.parsing

import org.antlr.v4.runtime._

/**
  * Error Listener that reports errors to the user based on any syntax errors found
  */
class ErrorListener extends BaseErrorListener {

  override def syntaxError(recognizer: Recognizer[_, _], offendingSymbol: scala.Any,
                           line: Int, charPositionInLine: Int, msg: String, e: RecognitionException): Unit = {

    System.err.println("Compile halted because of a lexical error!")
    val token = offendingSymbol.asInstanceOf[Token]
    val tokenStream = recognizer.getInputStream.asInstanceOf[CommonTokenStream]
    val lines = tokenStream.getTokenSource.getInputStream.toString.split('\n')

    if(msg.contains("if") || msg.contains("else")) {
      System.err.println("There is most likely as issue with your if/else conditional \n on Line: " + token.getLine)
    }
    else {
      System.err.println("ON LINE: " + token.getLine + ":" + charPositionInLine + " :" + " There is an issue : " + msg)
    }
    System.err.println(lines(token.getLine - 2).trim)

    for(i <- 0 until charPositionInLine) {
      print(" ")
    }

    for(i <- token.getStartIndex until token.getStopIndex) print("^")
    println()

    System.exit(1)

  }
}
