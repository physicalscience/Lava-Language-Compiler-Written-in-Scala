// Generated from /home/brandon/IdeaProjects/ScalaLava/src/main/lexicalanalysis/Lava.g4 by ANTLR 4.5.3
package main.abstractsyntax;
import main.parsing.LavaParser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LavaParser}.
 */
public interface LavaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LavaParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(LavaParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(LavaParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#mainClass}.
	 * @param ctx the parse tree
	 */
	void enterMainClass(LavaParser.MainClassContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#mainClass}.
	 * @param ctx the parse tree
	 */
	void exitMainClass(LavaParser.MainClassContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#classDec}.
	 * @param ctx the parse tree
	 */
	void enterClassDec(LavaParser.ClassDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#classDec}.
	 * @param ctx the parse tree
	 */
	void exitClassDec(LavaParser.ClassDecContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#extension}.
	 * @param ctx the parse tree
	 */
	void enterExtension(LavaParser.ExtensionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#extension}.
	 * @param ctx the parse tree
	 */
	void exitExtension(LavaParser.ExtensionContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#varDec}.
	 * @param ctx the parse tree
	 */
	void enterVarDec(LavaParser.VarDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#varDec}.
	 * @param ctx the parse tree
	 */
	void exitVarDec(LavaParser.VarDecContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#methodDec}.
	 * @param ctx the parse tree
	 */
	void enterMethodDec(LavaParser.MethodDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#methodDec}.
	 * @param ctx the parse tree
	 */
	void exitMethodDec(LavaParser.MethodDecContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#localDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterLocalDeclaration(LavaParser.LocalDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#localDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitLocalDeclaration(LavaParser.LocalDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFieldDeclaration(LavaParser.FieldDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFieldDeclaration(LavaParser.FieldDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#methodReturn}.
	 * @param ctx the parse tree
	 */
	void enterMethodReturn(LavaParser.MethodReturnContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#methodReturn}.
	 * @param ctx the parse tree
	 */
	void exitMethodReturn(LavaParser.MethodReturnContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#methodParams}.
	 * @param ctx the parse tree
	 */
	void enterMethodParams(LavaParser.MethodParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#methodParams}.
	 * @param ctx the parse tree
	 */
	void exitMethodParams(LavaParser.MethodParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#methodVarDec}.
	 * @param ctx the parse tree
	 */
	void enterMethodVarDec(LavaParser.MethodVarDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#methodVarDec}.
	 * @param ctx the parse tree
	 */
	void exitMethodVarDec(LavaParser.MethodVarDecContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(LavaParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(LavaParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#returnType}.
	 * @param ctx the parse tree
	 */
	void enterReturnType(LavaParser.ReturnTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#returnType}.
	 * @param ctx the parse tree
	 */
	void exitReturnType(LavaParser.ReturnTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#aReturn}.
	 * @param ctx the parse tree
	 */
	void enterAReturn(LavaParser.AReturnContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#aReturn}.
	 * @param ctx the parse tree
	 */
	void exitAReturn(LavaParser.AReturnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blockStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(LavaParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blockStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(LavaParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifElseStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIfElseStatement(LavaParser.IfElseStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifElseStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIfElseStatement(LavaParser.IfElseStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifError}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIfError(LavaParser.IfErrorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifError}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIfError(LavaParser.IfErrorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(LavaParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(LavaParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code errorWhile}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterErrorWhile(LavaParser.ErrorWhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code errorWhile}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitErrorWhile(LavaParser.ErrorWhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code consolePrint}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterConsolePrint(LavaParser.ConsolePrintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code consolePrint}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitConsolePrint(LavaParser.ConsolePrintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code errorConsole}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterErrorConsole(LavaParser.ErrorConsoleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code errorConsole}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitErrorConsole(LavaParser.ErrorConsoleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignmentStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentStatement(LavaParser.AssignmentStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignmentStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentStatement(LavaParser.AssignmentStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAssignmentStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterArrayAssignmentStatement(LavaParser.ArrayAssignmentStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAssignmentStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitArrayAssignmentStatement(LavaParser.ArrayAssignmentStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void enterIfBlock(LavaParser.IfBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void exitIfBlock(LavaParser.IfBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#eBlock}.
	 * @param ctx the parse tree
	 */
	void enterEBlock(LavaParser.EBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#eBlock}.
	 * @param ctx the parse tree
	 */
	void exitEBlock(LavaParser.EBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link LavaParser#wBlock}.
	 * @param ctx the parse tree
	 */
	void enterWBlock(LavaParser.WBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link LavaParser#wBlock}.
	 * @param ctx the parse tree
	 */
	void exitWBlock(LavaParser.WBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanTrueExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterBooleanTrueExpression(LavaParser.BooleanTrueExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanTrueExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitBooleanTrueExpression(LavaParser.BooleanTrueExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code objectInstantiationExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterObjectInstantiationExpression(LavaParser.ObjectInstantiationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code objectInstantiationExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitObjectInstantiationExpression(LavaParser.ObjectInstantiationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intLiteralExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterIntLiteralExpression(LavaParser.IntLiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intLiteralExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitIntLiteralExpression(LavaParser.IntLiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code methodCallExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterMethodCallExpression(LavaParser.MethodCallExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code methodCallExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitMethodCallExpression(LavaParser.MethodCallExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(LavaParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(LavaParser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyExpression(LavaParser.MultiplyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyExpression(LavaParser.MultiplyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code greaterThanExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterGreaterThanExpression(LavaParser.GreaterThanExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code greaterThanExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitGreaterThanExpression(LavaParser.GreaterThanExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bracketExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterBracketExpression(LavaParser.BracketExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bracketExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitBracketExpression(LavaParser.BracketExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExperssion}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterParenExperssion(LavaParser.ParenExperssionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExperssion}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitParenExperssion(LavaParser.ParenExperssionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lengthIdentifier}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLengthIdentifier(LavaParser.LengthIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lengthIdentifier}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLengthIdentifier(LavaParser.LengthIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classAssignment}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterClassAssignment(LavaParser.ClassAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classAssignment}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitClassAssignment(LavaParser.ClassAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(LavaParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(LavaParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterAddExpression(LavaParser.AddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitAddExpression(LavaParser.AddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayInitializerExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterArrayInitializerExpression(LavaParser.ArrayInitializerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayInitializerExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitArrayInitializerExpression(LavaParser.ArrayInitializerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subtractionExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterSubtractionExpression(LavaParser.SubtractionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subtractionExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitSubtractionExpression(LavaParser.SubtractionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lessThanExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLessThanExpression(LavaParser.LessThanExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lessThanExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLessThanExpression(LavaParser.LessThanExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanFalseExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterBooleanFalseExpression(LavaParser.BooleanFalseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanFalseExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitBooleanFalseExpression(LavaParser.BooleanFalseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalIdentifierExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralIdentifierExpression(LavaParser.LiteralIdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalIdentifierExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralIdentifierExpression(LavaParser.LiteralIdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code floatExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterFloatExpression(LavaParser.FloatExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code floatExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitFloatExpression(LavaParser.FloatExpressionContext ctx);
}