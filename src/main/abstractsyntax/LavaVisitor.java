// Generated from /home/brandon/IdeaProjects/ScalaLava/src/main/lexicalanalysis/Lava.g4 by ANTLR 4.5.3
package main.abstractsyntax;
import main.parsing.LavaParser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LavaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LavaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LavaParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(LavaParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#mainClass}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMainClass(LavaParser.MainClassContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#classDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDec(LavaParser.ClassDecContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#extension}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtension(LavaParser.ExtensionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#varDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDec(LavaParser.VarDecContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#methodDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodDec(LavaParser.MethodDecContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#localDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalDeclaration(LavaParser.LocalDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldDeclaration(LavaParser.FieldDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#methodReturn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodReturn(LavaParser.MethodReturnContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#methodParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodParams(LavaParser.MethodParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#methodVarDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodVarDec(LavaParser.MethodVarDecContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(LavaParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#returnType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnType(LavaParser.ReturnTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#aReturn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAReturn(LavaParser.AReturnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blockStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(LavaParser.BlockStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifElseStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfElseStatement(LavaParser.IfElseStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifError}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfError(LavaParser.IfErrorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(LavaParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code errorWhile}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitErrorWhile(LavaParser.ErrorWhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code consolePrint}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConsolePrint(LavaParser.ConsolePrintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code errorConsole}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitErrorConsole(LavaParser.ErrorConsoleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignmentStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentStatement(LavaParser.AssignmentStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayAssignmentStatement}
	 * labeled alternative in {@link LavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayAssignmentStatement(LavaParser.ArrayAssignmentStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#ifBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfBlock(LavaParser.IfBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#eBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEBlock(LavaParser.EBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LavaParser#wBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWBlock(LavaParser.WBlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanTrueExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanTrueExpression(LavaParser.BooleanTrueExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code objectInstantiationExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjectInstantiationExpression(LavaParser.ObjectInstantiationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intLiteralExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntLiteralExpression(LavaParser.IntLiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code methodCallExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodCallExpression(LavaParser.MethodCallExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpression(LavaParser.NotExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplyExpression(LavaParser.MultiplyExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code greaterThanExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreaterThanExpression(LavaParser.GreaterThanExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bracketExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBracketExpression(LavaParser.BracketExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExperssion}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExperssion(LavaParser.ParenExperssionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lengthIdentifier}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLengthIdentifier(LavaParser.LengthIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classAssignment}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassAssignment(LavaParser.ClassAssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(LavaParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddExpression(LavaParser.AddExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayInitializerExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayInitializerExpression(LavaParser.ArrayInitializerExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subtractionExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtractionExpression(LavaParser.SubtractionExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lessThanExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessThanExpression(LavaParser.LessThanExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanFalseExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanFalseExpression(LavaParser.BooleanFalseExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalIdentifierExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralIdentifierExpression(LavaParser.LiteralIdentifierExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code floatExpression}
	 * labeled alternative in {@link LavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatExpression(LavaParser.FloatExpressionContext ctx);
}